use js_sys::Reflect;
use wasm_bindgen::JsValue;

#[derive(Debug)]
pub enum Evt {
    ShowControlsClicked,
    PlayerButtonDown { player: u32, button: String },
    PlayerButtonUp { player: u32, button: String },
    GeneralButtonDown { button: String },
    GeneralButtonUp { button: String },
    KeyDown { key: String },
    KeyUp { key: String },
}

impl Evt {
    pub fn from_js_value(ev: JsValue) -> Self {
        let ev_name = prop_string(&ev, "name");

        match ev_name.as_str() {
            "show_controls_clicked" => Self::ShowControlsClicked,
            "player_button_down" => Self::PlayerButtonDown {
                player: prop_u32(&ev, "player"),
                button: prop_string(&ev, "button"),
            },
            "player_button_up" => Self::PlayerButtonUp {
                player: 0,
                button: prop_string(&ev, "button"),
            },
            "general_button_down" => Self::GeneralButtonDown {
                button: prop_string(&ev, "button"),
            },
            "general_button_up" => Self::GeneralButtonUp {
                button: prop_string(&ev, "button"),
            },
            "key_down" => Self::KeyDown {
                key: prop_string(&ev, "key"),
            },
            "key_up" => Self::KeyUp {
                key: prop_string(&ev, "key"),
            },
            _ => panic!("Unknown event name: {}", ev_name),
        }
    }
}

fn prop_string(ev: &JsValue, name: &'static str) -> String {
    Reflect::get(ev, &name.into())
        .expect("Event was not an object!")
        .as_string()
        .expect("property was not a string!")
}

fn prop_u32(ev: &JsValue, name: &'static str) -> u32 {
    Reflect::get(ev, &name.into())
        .expect("Event was not an object!")
        .as_f64()
        .expect("property was not a number!") as u32
}
