use image::codecs::png::PngDecoder;
use image::DynamicImage;

pub fn decode_image(png_bytes: &[u8]) -> DynamicImage {
    DynamicImage::from_decoder(
        PngDecoder::new(png_bytes).expect("Unable to decode png"),
    )
    .expect("Unable to create image from png")
}
