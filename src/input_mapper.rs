use crate::input::Input;

#[derive(Default)]
pub struct InputMapper {}

impl InputMapper {
    pub fn player_button_down(&self, player: u32, button: String) -> Input {
        Input::PlayerInputDown { player, button }
    }

    pub fn player_button_up(&self, player: u32, button: String) -> Input {
        Input::PlayerInputUp { player, button }
    }

    pub fn general_button_down(&self, button: String) -> Input {
        Input::GeneralInputDown { button }
    }

    pub fn general_button_up(&self, button: String) -> Input {
        Input::GeneralInputUp { button }
    }

    pub fn key_down(&self, key: String) -> Input {
        if let Some(button) = self.general_mapped_key(&key) {
            Input::GeneralInputDown {
                button: button.to_owned(),
            }
        } else if let Some((player, button)) = self.player_mapped_key(&key) {
            Input::PlayerInputDown {
                player,
                button: button.to_owned(),
            }
        } else {
            Input::UnmappedKeyDown { key }
        }
    }

    pub fn key_up(&self, key: String) -> Input {
        if let Some(button) = self.general_mapped_key(&key) {
            Input::GeneralInputUp {
                button: button.to_owned(),
            }
        } else if let Some((player, button)) = self.player_mapped_key(&key) {
            Input::PlayerInputUp {
                player,
                button: button.to_owned(),
            }
        } else {
            Input::UnmappedKeyUp { key }
        }
    }

    fn general_mapped_key(&self, key: &str) -> Option<&'static str> {
        // TODO: allow remappable keys
        // TODO: allow naming the keys e.g. "Fire", "Jump"
        match key {
            "Enter" => Some("Start"),
            "Escape" => Some("Menu"),
            _ => None,
        }
    }

    fn player_mapped_key(&self, key: &str) -> Option<(u32, &'static str)> {
        // TODO: allow remappable keys
        // TODO: allow the game to define which player inputs exist
        // TODO: allow naming the keys e.g. "Fire", "Jump"
        match key {
            "ArrowUp" => Some((0, "ArrowUp")),
            "ArrowDown" => Some((0, "ArrowDown")),
            "ArrowLeft" => Some((0, "ArrowLeft")),
            "ArrowRight" => Some((0, "ArrowRight")),
            " " => Some((0, "ButtonA")),
            "w" => Some((1, "ArrowUp")),
            "s" => Some((1, "ArrowDown")),
            "a" => Some((1, "ArrowLeft")),
            "d" => Some((1, "ArrowRight")),
            "c" => Some((1, "ButtonA")),
            _ => None,
        }
    }
}
