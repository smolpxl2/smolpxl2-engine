use crate::{input::Input, input_filter::InputFilter};

pub struct InputProcessor {
    filters: Vec<Box<dyn InputFilter>>,
}

impl InputProcessor {
    pub fn new() -> Self {
        Self {
            filters: Vec::new(),
        }
    }

    pub fn layer(self, filter: Box<dyn InputFilter>) -> Self {
        let mut filters = self.filters;
        filters.push(filter);
        Self { filters }
    }

    pub(crate) fn process(&mut self, mut inputs: Vec<Input>) -> Vec<Input> {
        for filter in &mut self.filters {
            inputs = filter.process(inputs);
        }
        inputs
    }
}

#[cfg(test)]
pub(crate) mod tests {

    use crate::{input::Input, input_filter::InputFilter};

    use super::InputProcessor;

    #[test]
    fn no_filters_passes_through_inputs_unchanged() {
        let inputs = vec![i("a"), i("b"), i("c")];
        let mut processor = InputProcessor::new();

        assert_eq!(processor.process(inputs), [i("a"), i("b"), i("c")]);
    }

    #[test]
    fn filtering_can_remove_items() {
        let inputs = vec![i("b"), i("a"), i("b"), i("c")];
        let mut processor = InputProcessor::new().layer(Box::new(NoBs {}));
        assert_eq!(processor.process(inputs), [i("a"), i("c")]);
    }

    #[test]
    fn filters_can_hold_state() {
        let inputs = vec![i("b"), i("a"), i("b"), i("c")];

        let mut processor =
            InputProcessor::new().layer(Box::new(FirstOnly::new()));

        assert_eq!(processor.process(inputs), [i("b")]);
    }

    #[test]
    fn filtering_can_create_more_items() {
        let inputs = vec![i("c"), i("a"), i("c"), i("a"), i("c")];
        let mut processor = InputProcessor::new().layer(Box::new(XyForC {}));

        assert_eq!(
            processor.process(inputs),
            [
                i("x"),
                i("y"),
                i("a"),
                i("x"),
                i("y"),
                i("a"),
                i("x"),
                i("y")
            ]
        );
    }

    #[test]
    fn can_use_multiple_filters() {
        let inputs = vec![i("c"), i("b"), i("c"), i("a"), i("c")];
        let mut processor = InputProcessor::new()
            .layer(Box::new(XyForC {}))
            .layer(Box::new(NoBs {}));

        assert_eq!(
            processor.process(inputs),
            [i("x"), i("y"), i("x"), i("y"), i("a"), i("x"), i("y")]
        );
    }

    #[test]
    fn filters_stack() {
        let inputs = vec![i("c"), i("b"), i("c"), i("a"), i("c")];
        let mut processor = InputProcessor::new()
            .layer(Box::new(XyForC {}))
            .layer(Box::new(YbToZ::new()));

        assert_eq!(
            buts(processor.process(inputs)),
            buts([i("x"), i("z"), i("x"), i("y"), i("a"), i("x"), i("y")])
        );
    }

    struct NoBs {}

    impl InputFilter for NoBs {
        fn process(&mut self, inputs: Vec<Input>) -> Vec<Input> {
            inputs
                .into_iter()
                .filter(|input| match &input {
                    Input::PlayerInputDown { button, .. } => button != "b",
                    _ => true,
                })
                .collect()
        }
    }

    struct XyForC {}

    impl InputFilter for XyForC {
        fn process(&mut self, inputs: Vec<Input>) -> Vec<Input> {
            let mut ret = vec![];
            for input in inputs {
                match &input {
                    Input::PlayerInputDown { button, .. } if button == "c" => {
                        ret.push(i("x"));
                        ret.push(i("y"));
                    }
                    _ => {
                        ret.push(input);
                    }
                }
            }
            ret
        }
    }

    struct FirstOnly {}

    impl FirstOnly {
        fn new() -> Self {
            Self {}
        }
    }

    impl InputFilter for FirstOnly {
        fn process(&mut self, inputs: Vec<Input>) -> Vec<Input> {
            inputs.into_iter().next().into_iter().collect()
        }
    }

    struct YbToZ {}

    impl YbToZ {
        fn new() -> Self {
            Self {}
        }
    }

    impl InputFilter for YbToZ {
        fn process(&mut self, inputs: Vec<Input>) -> Vec<Input> {
            let mut ret = Vec::new();
            let mut prev_was_y = false;
            for input in inputs {
                if let Input::PlayerInputDown { button, .. } = &input {
                    if prev_was_y {
                        if button == "b" {
                            prev_was_y = false;
                            ret.push(i("z"))
                        } else {
                            if button != "y" {
                                prev_was_y = false;
                            }
                            ret.push(i("y"));
                            ret.push(input);
                        }
                    } else {
                        if button == "y" {
                            prev_was_y = true;
                        } else {
                            ret.push(input);
                        }
                    }
                } else {
                    prev_was_y = false;
                    ret.push(input);
                }
            }

            if prev_was_y {
                ret.push(i("y"));
            }

            ret
        }
    }

    fn i(key: &str) -> Input {
        Input::PlayerInputDown {
            player: 0,
            button: key.to_owned(),
        }
    }

    pub fn buts(inputs: impl IntoIterator<Item = Input>) -> String {
        inputs
            .into_iter()
            .map(|input| {
                if let Input::PlayerInputDown { button, .. } = input {
                    button.to_owned()
                } else {
                    format!("{input:?}")
                }
            })
            .collect()
    }
}
