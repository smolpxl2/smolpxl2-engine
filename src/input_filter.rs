use crate::input::Input;

pub trait InputFilter {
    fn process(&mut self, inputs: Vec<Input>) -> Vec<Input>;
}
