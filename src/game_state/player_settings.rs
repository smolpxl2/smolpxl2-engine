use std::collections::HashMap;

use crate::{
    color::Color,
    game_state::custom_setting::{CustomSetting, CustomSettingValue},
};

use super::{custom_setting::CustomSettingType, player_input::PlayerInputs};

#[derive(Debug)]
pub struct PlayerSettings {
    pub min_players: usize,
    pub max_players: usize,
    pub inputs: PlayerInputs,
    pub player_details: Vec<PlayerDetail>,
    pub custom_settings: Vec<CustomSetting>,
}

impl Default for PlayerSettings {
    fn default() -> Self {
        Self {
            min_players: 1,
            max_players: 8,
            inputs: PlayerInputs::arrows_and_fire(),
            player_details: vec![],
            custom_settings: vec![],
        }
    }
}

#[derive(Debug, Default)]
pub struct PlayerDetail {
    pub name: Option<String>,
    pub color: Option<Color>,
    pub custom_values: CustomValuesMap,
}

#[derive(Debug, Default)]
pub struct CustomValuesMap(HashMap<String, CustomSettingValue>);

impl CustomValuesMap {
    pub fn get(
        &self,
        name: &str,
        typ: &CustomSettingType,
    ) -> CustomSettingValue {
        self.0.get(name).cloned().unwrap_or(typ.default())
    }

    pub fn insert(&mut self, name: &str, value: CustomSettingValue) {
        self.0.insert(name.to_owned(), value);
    }
}
