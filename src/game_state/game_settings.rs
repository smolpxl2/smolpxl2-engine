pub struct GameSettings {
    pub size: ScreenSize,
    pub smolpxl_bar: bool,
    pub default_controls_mode: ControlsMode,
    pub frame_time_ms: u64,
}

impl GameSettings {
    pub fn set_fps(&mut self, fps: u64) {
        self.frame_time_ms = 1000 / fps;
    }
}

impl Default for GameSettings {
    fn default() -> Self {
        Self {
            size: ScreenSize {
                width: 30,
                height: 30,
            },
            smolpxl_bar: false,
            default_controls_mode: ControlsMode::None,
            frame_time_ms: 1000 / 60,
        }
    }
}

pub struct ScreenSize {
    pub width: u32,
    pub height: u32,
}

pub enum ControlsMode {
    None,
    Minimized,
    Overlay,
    //Below,
    //Beside,
}

impl ControlsMode {
    pub fn as_str(&self) -> &'static str {
        match self {
            ControlsMode::None => "none",
            ControlsMode::Minimized => "minimized",
            ControlsMode::Overlay => "overlay",
        }
    }
}
