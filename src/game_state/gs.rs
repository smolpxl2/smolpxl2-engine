use rand::{rngs::ThreadRng, Rng};

use crate::{input::Input, inputs::Inputs, title_page::TitlePage};

use super::{
    editable::Editable,
    game_mode::GameMode,
    game_settings::GameSettings,
    menu::Menu,
    player_settings::{PlayerDetail, PlayerSettings},
};

pub struct GameState<R = ThreadRng>
where
    R: Rng,
{
    /**
     * The name of this game
     */
    pub name: Editable<Option<String>>,

    /**
     * Whether we are displaying the title screens, menu, or game
     */
    pub game_mode: Editable<GameMode>,

    /**
     * The game mode before we switched to menu
     */
    pub pre_menu_game_mode: GameMode,

    /**
     * Any user input not yet dealt with. To read the next input
     * (in the order they were pressed), use game_state.inputs.next().
     */
    pub inputs: Inputs,

    /**
     * How many players are active, their names, controls etc.
     */
    pub player_settings: Editable<PlayerSettings>,

    /**
     * Which non-player-specific controls to display e.g. "Start", "Menu".
     */
    pub general_controls_settings: Editable<Vec<String>>,

    /**
     * Screen size, touch controls, FPS etc.
     */
    pub game_settings: Editable<GameSettings>,

    /**
     * The pages shown when the game starts
     */
    pub title_pages: Editable<Vec<TitlePage>>,

    /**
     * The multi-level menu screens the user can navigate
     */
    pub menu: Editable<Menu>,

    /**
     * A random number generator
     */
    pub rng: R,
}

impl GameState {
    pub fn new<R>(rng: R) -> GameState<R>
    where
        R: Rng,
    {
        GameState::<R> {
            name: Editable::new(None),
            game_mode: Editable::new(GameMode::Title(0)),
            pre_menu_game_mode: GameMode::Title(1),
            inputs: Default::default(),
            player_settings: Editable::new(Default::default()),
            general_controls_settings: Editable::new(vec![
                "Start".to_owned(),
                "Menu".to_owned(),
            ]),
            game_settings: Editable::new(Default::default()),
            title_pages: Editable::new(Vec::new()),
            menu: Editable::new(Menu::new("Menu")),
            rng,
        }
    }
}

impl<R> GameState<R>
where
    R: Rng,
{
    pub fn add_player(&mut self) {
        let player_settings = self.player_settings.edit();
        let new_player_index = player_settings.player_details.len();
        player_settings.player_details.push(PlayerDetail::default());
        self.inputs.push(Input::PlayerCreated {
            player: new_player_index as u32,
        });
    }
}

impl Default for GameState {
    fn default() -> Self {
        Self::new(rand::thread_rng())
    }
}
