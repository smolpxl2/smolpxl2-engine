#[derive(Debug)]
pub struct Editable<T> {
    inner: T,
    edited: bool,
}

impl<T> Editable<T> {
    pub fn new(inner: T) -> Self {
        Self {
            inner,
            edited: false,
        }
    }

    pub fn edit(&mut self) -> &mut T {
        self.edited = true;
        &mut self.inner
    }

    pub fn view(&self) -> &T {
        &self.inner
    }

    pub fn reset_edited(&mut self) -> bool {
        let ret = self.edited;
        self.edited = false;
        ret
    }

    pub fn is_edited(&self) -> bool {
        self.edited
    }
}
