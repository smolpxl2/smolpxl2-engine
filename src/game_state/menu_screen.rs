use super::menu::MenuSettingState;

/// Everything you need to draw a menu on screen
#[derive(Debug, PartialEq)]
pub struct MenuScreen<'a> {
    pub title: &'a str,
    pub items: Vec<&'a str>,
    pub selected_item: usize,
    pub active_setting: Option<MenuSettingState>,
}

impl<'a> MenuScreen<'a> {
    pub fn new(
        title: &'a str,
        items: Vec<&'a str>,
        selected_item: usize,
        active_setting: Option<MenuSettingState>,
    ) -> Self {
        Self {
            title,
            items,
            selected_item,
            active_setting,
        }
    }
}
