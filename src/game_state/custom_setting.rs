use std::fmt::Display;

#[derive(Debug)]
pub struct CustomSetting {
    pub name: String,
    pub display_name: String,
    pub typ: CustomSettingType,
}

impl CustomSetting {
    pub fn new(name: &str, display_name: &str) -> Self {
        Self {
            name: name.to_owned(),
            display_name: display_name.to_owned(),
            typ: CustomSettingType::String,
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum CustomSettingType {
    String,
}

impl CustomSettingType {
    pub fn default(&self) -> CustomSettingValue {
        match self {
            CustomSettingType::String => {
                CustomSettingValue::String(String::default())
            }
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum CustomSettingValue {
    String(String),
}

impl Display for CustomSettingValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::String(s) => f.write_str(s),
        }
    }
}
