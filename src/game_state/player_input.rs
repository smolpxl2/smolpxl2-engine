use std::iter::once;

#[derive(Debug, PartialEq, strum::Display)]
pub enum PlayerInput {
    ArrowUp,
    ArrowDown,
    ArrowLeft,
    ArrowRight,
    ButtonA,
    ButtonB,
    ButtonC,
    ButtonD,
}

#[derive(Debug)]
pub struct PlayerInputs(Vec<PlayerInput>);

impl PlayerInputs {
    pub fn arrows_and_fire() -> Self {
        Self(vec![
            PlayerInput::ArrowUp,
            PlayerInput::ArrowDown,
            PlayerInput::ArrowLeft,
            PlayerInput::ArrowRight,
            PlayerInput::ButtonA,
        ])
    }

    pub fn arrows() -> Self {
        Self(vec![
            PlayerInput::ArrowUp,
            PlayerInput::ArrowDown,
            PlayerInput::ArrowLeft,
            PlayerInput::ArrowRight,
        ])
    }

    pub fn to_ui_strings(&self) -> Vec<String> {
        let arrows = PlayerInputs::arrows();
        if arrows.0.iter().all(|i| self.0.contains(i)) {
            self.0
                .iter()
                .filter(|i| !arrows.0.contains(i))
                .map(ToString::to_string)
                .chain(once("ArrowKeys".to_owned()))
                .collect()
        } else {
            self.0.iter().map(ToString::to_string).collect()
        }
    }

    pub fn as_vec(&self) -> &Vec<PlayerInput> {
        &self.0
    }
}
