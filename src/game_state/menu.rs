use super::{custom_setting::CustomSettingValue, menu_screen::MenuScreen};

#[derive(Clone, Debug, PartialEq)]
pub struct MenuSettingState {
    pub description: String,
    pub display_name: String,
    pub key: String,
    pub value: CustomSettingValue,
    pub typ: MenuSettingType,
}

impl MenuSettingState {
    pub fn player_name(
        description: &str,
        display_name: &str,
        key: &str,
        value: &str,
        player_num: usize,
    ) -> Self {
        Self {
            description: description.to_owned(),
            display_name: display_name.to_owned(),
            key: key.to_owned(),
            value: CustomSettingValue::String(value.to_owned()),
            typ: MenuSettingType::PlayerName { player_num },
        }
    }

    pub(crate) fn player_custom_setting(
        description: &str,
        display_name: &str,
        key: &str,
        value: CustomSettingValue,
        player_num: usize,
    ) -> MenuSettingState {
        Self {
            description: description.to_owned(),
            display_name: display_name.to_owned(),
            key: key.to_owned(),
            value,
            typ: MenuSettingType::PlayerCustomSetting { player_num },
        }
    }

    pub fn into_menu_item(self) -> MenuItem {
        MenuItem {
            name: self.key.clone(),
            display_name: self.menu_display_name(),
            typ: match self.typ {
                MenuSettingType::PlayerName { player_num } => {
                    MenuItemType::Setting(MenuSettingState::player_name(
                        &self.description,
                        match &self.value {
                            CustomSettingValue::String(s) => s,
                        },
                        &self.key,
                        match &self.value {
                            CustomSettingValue::String(s) => s,
                        },
                        player_num,
                    ))
                }
                MenuSettingType::PlayerCustomSetting { player_num } => {
                    MenuItemType::Setting(
                        MenuSettingState::player_custom_setting(
                            &self.description,
                            match &self.value {
                                CustomSettingValue::String(s) => s,
                            },
                            &self.key,
                            self.value.clone(),
                            player_num,
                        ),
                    )
                }
            },
        }
    }

    fn menu_display_name(&self) -> String {
        let value = &self.value.to_string();

        if value.is_empty() {
            self.description.clone()
        } else if value.len() > 16 {
            format!("{}: {}\u{2026}", self.description, &value[..15])
        } else {
            format!("{}: {}", self.description, value)
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum MenuSettingType {
    PlayerName { player_num: usize },
    PlayerCustomSetting { player_num: usize },
}

#[derive(Debug)]
pub struct Menu {
    /// The indices within each sublevel that point to the current level the
    /// user is viewing. Guaranteed to be pointing at valid indices within each
    /// level's item list, and guaranteed to point to a MenuLevel.
    /// An empty Vec means the top level.
    /// TODO: validate these guarantees.
    current_level_path: Vec<usize>,

    /// The item within the current level that is selected.
    /// Guaranteed to be a valid index within the current level.
    /// TODO: validate this guarantee.
    selected_index: usize,

    /// The main menu. Contains other levels if there are submenus.
    top_level: MenuLevel,

    setting: Option<MenuSettingState>,
}

impl Menu {
    pub(crate) fn new(display_name: &str) -> Self {
        Self {
            current_level_path: Vec::new(),
            selected_index: 0,
            top_level: MenuLevel::new(display_name.to_owned()),
            setting: None,
        }
    }

    pub(crate) fn selected_item(&self) -> &MenuItem {
        self.current_level()
            .items()
            .get(self.selected_index)
            .expect("selected_item was invalid!")
    }

    pub(crate) fn current_screen(&self) -> MenuScreen {
        self.current_level()
            .screen_here(self.selected_index, self.active_setting())
    }

    fn active_setting(&self) -> Option<MenuSettingState> {
        self.setting.clone()
    }

    pub(crate) fn dialog_done(&mut self) {
        self.setting = None;
    }

    pub(crate) fn current_level(&self) -> &MenuLevel {
        if self.current_level_path.is_empty() {
            &self.top_level
        } else {
            self.top_level
                .at_level_sublevel_indexed(&self.current_level_path)
        }
    }

    /// Panics if location is not valid
    #[cfg(test)]
    pub(crate) fn append_sublevel(
        &mut self,
        location: &[&str],
        name: &str,
        display_name: &str,
    ) {
        self.top_level
            .find_sublevel_mut(location)
            .unwrap_or_else(|| panic!("Invalid location {location:?}"))
            .at_level_append_sublevel(name, display_name);
    }

    /// Panics if location is not valid
    #[cfg(test)]
    pub(crate) fn append_action(
        &mut self,
        location: &[&str],
        name: &str,
        display_name: &str,
        action: MenuAction,
    ) {
        self.top_level
            .find_sublevel_mut(location)
            .unwrap_or_else(|| panic!("Invalid location {location:?}"))
            .at_level_append_action(name, display_name, action);
    }

    pub(crate) fn append_item(&mut self, location: &[&str], item: MenuItem) {
        self.top_level
            .find_sublevel_mut(location)
            .unwrap_or_else(|| panic!("Invalid location {location:?}"))
            .at_level_append_item(item);
    }

    /// Panics if location is not valid
    #[cfg(test)]
    pub(crate) fn move_item_to_end(&mut self, location: &[&str]) {
        assert!(!location.is_empty(), "location was too short!");
        let last_in_loc = location.len() - 1;
        let parent_loc = &location[..last_in_loc];
        let parent = self.top_level.find_sublevel_mut(parent_loc).unwrap();
        let item = parent.at_level_remove_item(location[last_in_loc]);
        parent.at_level_append_item(item);
    }

    #[cfg(test)]
    pub(crate) fn has_item(&self, location: &[&str]) -> bool {
        if location.is_empty() {
            true
        } else {
            self.top_level.find(location).is_some()
        }
    }

    #[cfg(test)]
    pub(crate) fn get_item(&self, location: &[&str]) -> Option<&MenuItem> {
        if location.is_empty() {
            panic!("No item at the top level of a menu!");
        }
        self.top_level.find(location)
    }

    pub(crate) fn get_item_mut(
        &mut self,
        location: &[&str],
    ) -> Option<&mut MenuItem> {
        if location.is_empty() {
            panic!("No item at the top level of a menu!");
        }
        self.top_level.find_mut(location)
    }

    pub(crate) fn is_at_top(&self) -> bool {
        self.current_level_path.is_empty()
    }

    pub(crate) fn move_next(&mut self) {
        if self.selected_index + 1 < self.current_level().items.len() {
            self.selected_index += 1;
        }
    }

    pub(crate) fn move_prev(&mut self) {
        if self.selected_index > 0 {
            self.selected_index -= 1;
        }
    }

    pub(crate) fn move_out(&mut self) {
        self.selected_index = self.current_level_path.pop().unwrap_or(0);
    }

    /**
     * Go back to the top-level page and select the first item.
     */
    pub(crate) fn move_top(&mut self) {
        self.current_level_path.clear();
        self.selected_index = 0;
    }

    pub(crate) fn choose(&mut self) -> Option<MenuAction> {
        let item = self.selected_item();
        match &item.typ {
            MenuItemType::Sublevel(level) => {
                // Check that this sublevel has items. If not, we
                // will have no valid item at our new selected_index.
                if !level.items().is_empty() {
                    // It's safe to add our selected index to the path
                    // because we just checked that it points to a sublevel.
                    self.current_level_path.push(self.selected_index);
                    self.selected_index = 0;
                }
                // TODO: maybe we want empty sublevels to look disabled?
                // We do nothing if we try to choose them.
                None
            }
            MenuItemType::Setting(setting) => {
                self.setting = Some(setting.clone());
                None
            }
            MenuItemType::Action(action) => {
                let a = action.clone();
                self.do_action(&a);
                Some(a)
            }
        }
    }

    fn do_action(&mut self, action: &MenuAction) {
        match action {
            MenuAction::MenuOut => self.move_out(),
            MenuAction::AddPlayer => {} // Handled in engine
            MenuAction::BackToTitleScreen => {} // Handled in engine
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct MenuLevel {
    pub display_name: String,
    pub items: Vec<MenuItem>,
}

impl MenuLevel {
    pub fn new(display_name: String) -> Self {
        Self::from_items(display_name, Vec::new())
    }

    pub(crate) fn from_items(
        display_name: String,
        items: Vec<MenuItem>,
    ) -> Self {
        Self {
            display_name,
            items,
        }
    }

    pub fn set_display_name(&mut self, display_name: &str) {
        self.display_name = display_name.to_owned();
    }

    pub fn items(&self) -> &Vec<MenuItem> {
        &self.items
    }

    fn screen_here(
        &self,
        selected_item: usize,
        active_setting: Option<MenuSettingState>,
    ) -> MenuScreen {
        MenuScreen::new(
            &self.display_name,
            self.items.iter().map(|i| i.display_name()).collect(),
            selected_item,
            active_setting,
        )
    }

    #[cfg(test)]
    fn at_level_append_sublevel(
        &mut self,
        name: &str,
        display_name: &str,
    ) -> &mut MenuLevel {
        self.items.push(MenuItem::new(
            name.to_owned(),
            display_name.to_owned(),
            MenuItemType::Sublevel(MenuLevel::new(display_name.to_owned())),
        ));

        // Unwrap is fine because we just added it, so exists
        let item = self.items.last_mut().unwrap();

        match &mut item.typ {
            MenuItemType::Sublevel(ret) => ret,
            _ => panic!("Just-added item is not the expected type!"),
        }
    }

    #[cfg(test)]
    fn at_level_append_setting_string(
        &mut self,
        key: &str,
        setting: MenuSettingState,
    ) {
        // TODO: different types of setting
        // TODO: cleverer generation of display name (at least shorten)
        self.items.push(MenuItem::new(
            key.to_owned(),
            setting.display_name.clone(),
            MenuItemType::Setting(setting),
        ));
    }

    #[cfg(test)]
    fn at_level_append_action(
        &mut self,
        name: &str,
        display_name: &str,
        action: MenuAction,
    ) {
        self.items.push(MenuItem::new(
            name.to_owned(),
            display_name.to_owned(),
            MenuItemType::Action(action),
        ));
    }

    fn at_level_append_item(&mut self, item: MenuItem) {
        self.items.push(item);
    }

    #[cfg(test)]
    fn at_level_remove_item(&mut self, name: &str) -> MenuItem {
        let index = self
            .items
            .iter()
            .position(|item| item.name == name)
            .unwrap();
        self.items.remove(index)
    }

    #[cfg(test)]
    fn at_level_find(&self, name: &str) -> Option<&MenuItem> {
        self.items.iter().find(|item| item.name == name)
    }

    fn at_level_find_mut(&mut self, name: &str) -> Option<&mut MenuItem> {
        self.items.iter_mut().find(|item| item.name == name)
    }

    /// panics if location is empty
    #[cfg(test)]
    fn find(&self, location: &[&str]) -> Option<&MenuItem> {
        let Some((head, tail)) = location.split_first() else {
            panic!("MenuLevel::find called with empty location!");
        };

        let Some(menu_item) = self.at_level_find(head) else {
            // Couldn't find the first thing in location
            return None;
        };

        if tail.is_empty() {
            // We found the first item, and there are no more
            return Some(menu_item);
        }

        let MenuItemType::Sublevel(sublevel) = &menu_item.typ else {
            // We have more location parts but this item is not
            // a sublevel so we can't recurse into it - not found.
            return None;
        };

        // We found a sublevel: now recurse into it
        sublevel.find(tail)
    }

    /// panics if location is empty
    fn find_mut(&mut self, location: &[&str]) -> Option<&mut MenuItem> {
        let Some((head, tail)) = location.split_first() else {
            panic!("MenuLevel::find called with empty location!");
        };

        let Some(menu_item) = self.at_level_find_mut(head) else {
            // Couldn't find the first thing in location
            return None;
        };

        if tail.is_empty() {
            // We found the first item, and there are no more
            return Some(menu_item);
        }

        let MenuItemType::Sublevel(sublevel) = &mut menu_item.typ else {
            // We have more location parts but this item is not
            // a sublevel so we can't recurse into it - not found.
            return None;
        };

        // We found a sublevel: now recurse into it
        sublevel.find_mut(tail)
    }

    fn find_sublevel_mut(
        &mut self,
        location: &[&str],
    ) -> Option<&mut MenuLevel> {
        if location.is_empty() {
            Some(self)
        } else {
            self.find_mut(location)
                .and_then(|item| match &mut item.typ {
                    MenuItemType::Sublevel(sublevel) => Some(sublevel),
                    _ => None,
                })
        }
    }

    fn at_level_sublevel_indexed(&self, level_path: &[usize]) -> &MenuLevel {
        let Some((head, tail)) = level_path.split_first() else {
            return self;
        };

        let item = &self.items[*head];
        let MenuItemType::Sublevel(sublevel) = &item.typ else {
            panic!("item at index was not a sublevel!")
        };

        sublevel.at_level_sublevel_indexed(tail)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum MenuAction {
    AddPlayer,
    BackToTitleScreen,
    MenuOut,
}

#[derive(Debug, PartialEq)]
pub struct MenuItem {
    pub name: String,
    pub display_name: String,
    pub typ: MenuItemType,
}

impl MenuItem {
    pub fn new(name: String, display_name: String, typ: MenuItemType) -> Self {
        Self {
            name,
            display_name,
            typ,
        }
    }

    pub(crate) fn display_name(&self) -> &str {
        &self.display_name
    }
}

#[derive(Debug, PartialEq)]
pub enum MenuItemType {
    Sublevel(MenuLevel),
    Setting(MenuSettingState),
    Action(MenuAction),
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn can_add_and_find_sublevels() {
        let mut items = MenuLevel::new("m1".to_owned());
        items.at_level_append_sublevel("sub1", "Sub 1");
        items.at_level_append_sublevel("sub2", "Sub 2");
        items.at_level_append_sublevel("sub3", "Sub 3");

        assert!(items.at_level_find("sub2").is_some());
        assert!(items.at_level_find_mut("sub2").is_some());
        assert!(items.at_level_find("NOT EXIST").is_none());
        assert!(items.at_level_find_mut("NOT EXIST").is_none());
    }

    #[test]
    fn find_with_nonmatching_type_returns_none() {
        let mut items = MenuLevel::new("m2".to_owned());
        items.at_level_append_setting_string(
            "setting1",
            MenuSettingState::player_name(
                "Setting 1",
                "",
                "setting1",
                "val1",
                0,
            ),
        );
        items.at_level_append_setting_string(
            "setting2",
            MenuSettingState::player_name(
                "Setting 2",
                "",
                "setting2",
                "val2",
                1,
            ),
        );
        items.at_level_append_setting_string(
            "setting3",
            MenuSettingState::player_name(
                "Setting 3",
                "",
                "setting3",
                "val3",
                2,
            ),
        );

        assert!(items.at_level_find("sub2").is_none());
    }

    #[test]
    fn screen_for_menu_level() {
        let mut items = MenuLevel::new("My Menu".to_owned());
        items.at_level_append_setting_string(
            "setting1",
            MenuSettingState::player_name(
                "Setting 1",
                "Setting 1: val1",
                "setting1",
                "val1",
                0,
            ),
        );
        items.at_level_append_setting_string(
            "setting2",
            MenuSettingState::player_name(
                "Setting 2",
                "Setting 2: val2",
                "setting2",
                "val2",
                1,
            ),
        );
        items.at_level_append_setting_string(
            "setting3",
            MenuSettingState::player_name(
                "Setting 3",
                "Setting 3: val3",
                "setting3",
                "val3",
                2,
            ),
        );

        assert_eq!(
            items.screen_here(1, None),
            MenuScreen {
                title: "My Menu",
                items: vec![
                    "Setting 1: val1",
                    "Setting 2: val2",
                    "Setting 3: val3"
                ],
                selected_item: 1,
                active_setting: None,
            },
        );
    }

    #[test]
    fn defaults_to_top_level() {
        // Given a menu
        let menu = create_sublevel_menu("My Menu", 3, false);

        // When I ask for the current screen
        // Then it displays, and the first item is selected
        assert_eq!(
            menu.current_screen(),
            MenuScreen {
                title: "My Menu",
                items: vec!["Item 1", "Item 2", "Item 3"],
                selected_item: 0,
                active_setting: None,
            }
        );
    }

    #[test]
    fn can_move_next() {
        // Given a menu
        let mut menu = create_sublevel_menu("My Menu", 3, false);

        // When I move next
        menu.move_next();

        // Then it still renders, and the second item is selected
        assert_eq!(
            menu.current_screen(),
            MenuScreen {
                title: "My Menu",
                items: vec!["Item 1", "Item 2", "Item 3"],
                selected_item: 1,
                active_setting: None,
            }
        );
    }

    #[test]
    fn moving_next_off_end_has_no_effect() {
        // Given a menu
        let mut menu = create_sublevel_menu("My Menu", 3, false);

        // When I move next a lot of times
        menu.move_next();
        menu.move_next();
        menu.move_next();
        menu.move_next();

        // Then I end up at the end, not off the end
        assert_eq!(menu.current_screen().selected_item, 2)
    }

    #[test]
    fn moving_next_in_empty_submenu_is_ok() {
        // Given an empty menu
        let mut menu = Menu::new("my menu");

        // When I move next a lot of times
        menu.move_next();
        menu.move_next();
        menu.move_next();
        menu.move_next();

        // Then nothing has gone wrong
        assert_eq!(menu.current_screen().selected_item, 0)
    }

    #[test]
    fn can_move_prev() {
        // Given a menu with item 2 selected
        let mut menu = create_sublevel_menu("Things", 4, false);
        menu.move_next();
        menu.move_next();
        menu.move_next();
        assert_eq!(menu.current_screen().selected_item, 3);

        // When I move to previous
        menu.move_prev();

        // Then it still renders, and item 2 is selected
        assert_eq!(
            menu.current_screen(),
            MenuScreen {
                title: "Things",
                items: vec!["Item 1", "Item 2", "Item 3", "Item 4"],
                selected_item: 2,
                active_setting: None,
            }
        );
    }

    #[test]
    fn moving_prev_off_beginning_has_no_effect() {
        // Given a menu
        let mut menu = create_sublevel_menu("My Menu", 3, false);

        // When I prev next a lot of times
        menu.move_prev();
        menu.move_prev();
        menu.move_prev();
        menu.move_prev();

        // Then I end up at the beginning
        assert_eq!(menu.current_screen().selected_item, 0)
    }

    #[test]
    fn finding_item_at_top_level() {
        let menu = create_sublevel_menu("m", 4, false);
        assert!(menu.has_item(&["Item 2"]));
        assert!(!menu.has_item(&["MISSING"]));
    }

    #[test]
    fn reports_at_top_when_not_in_sublevel() {
        let mut menu = create_sublevel_menu("m", 4, true);
        assert!(menu.is_at_top());

        menu.choose();
        assert!(!menu.is_at_top());

        menu.choose();
        assert!(!menu.is_at_top());

        menu.move_out();
        menu.move_out();
        assert!(menu.is_at_top());
    }

    #[test]
    fn has_item_with_empty_location_returns_true() {
        let menu = create_sublevel_menu("m", 2, false);
        assert!(menu.has_item(&[]));
    }

    #[test]
    fn finding_item_at_lower_level() {
        let menu = create_sublevel_menu("m", 3, true);
        assert!(menu.has_item(&["Item 2", "Item 2 3"]));
        assert!(!menu.has_item(&["Item 2", "MISSING"]));
    }

    #[test]
    fn screen_at_lower_level() {
        let mut menu = create_sublevel_menu("m", 3, true);
        menu.move_next();
        menu.move_next();
        menu.choose();
        assert_eq!(
            menu.current_screen(),
            MenuScreen {
                title: "Item 3",
                items: vec!["Item 3 1", "Item 3 2", "Item 3 3"],
                selected_item: 0,
                active_setting: None,
            }
        );
    }

    #[test]
    fn top_level_actions_appear_on_screen() {
        let mut menu = Menu::new("Action menu");
        menu.append_action(&[], "act1", "Action 1", MenuAction::MenuOut);
        menu.append_action(&[], "act2", "Action 2", MenuAction::MenuOut);
        menu.append_action(&[], "act3", "Action 3", MenuAction::MenuOut);
        menu.move_next();

        assert_eq!(
            menu.current_screen(),
            MenuScreen {
                title: "Action menu",
                items: vec!["Action 1", "Action 2", "Action 3"],
                selected_item: 1,
                active_setting: None,
            }
        );
    }

    #[test]
    fn lower_level_actions_appear_on_screen() {
        let mut menu = Menu::new("Action menu");
        menu.append_sublevel(&[], "sub1", "sub 1");
        menu.append_sublevel(&["sub1"], "sub1a", "sub 1a");
        menu.append_sublevel(&["sub1", "sub1a"], "sub1ai", "sub 1ai");
        menu.append_action(
            &["sub1", "sub1a", "sub1ai"],
            "act1",
            "Action 1",
            MenuAction::MenuOut,
        );
        menu.append_action(
            &["sub1", "sub1a", "sub1ai"],
            "act2",
            "Action 2",
            MenuAction::MenuOut,
        );
        menu.append_action(
            &["sub1", "sub1a", "sub1ai"],
            "act3",
            "Action 3",
            MenuAction::MenuOut,
        );
        menu.choose();
        menu.choose();
        menu.choose();
        menu.move_next();

        assert_eq!(
            menu.current_screen(),
            MenuScreen {
                title: "sub 1ai",
                items: vec!["Action 1", "Action 2", "Action 3"],
                selected_item: 1,
                active_setting: None,
            }
        );
    }

    #[test]
    fn can_move_item_to_end() {
        let mut menu = create_sublevel_menu("x", 4, true);
        menu.move_item_to_end(&["Item 2", "Item 2 2"]);
        menu.move_next();
        menu.choose();

        assert_eq!(
            menu.current_screen(),
            MenuScreen {
                title: "Item 2",
                items: vec!["Item 2 1", "Item 2 3", "Item 2 4", "Item 2 2"],
                selected_item: 0,
                active_setting: None,
            }
        );

        menu.move_item_to_end(&["Item 2", "Item 2 1"]);

        assert_eq!(
            menu.current_screen(),
            MenuScreen {
                title: "Item 2",
                items: vec!["Item 2 3", "Item 2 4", "Item 2 2", "Item 2 1"],
                selected_item: 0,
                active_setting: None,
            }
        );
    }

    #[test]
    fn moving_last_item_to_end_has_no_effect() {
        let mut menu = create_sublevel_menu("x", 4, true);
        menu.move_next();
        menu.move_next();
        menu.choose();
        menu.move_item_to_end(&["Item 3", "Item 3 4"]);

        assert_eq!(
            menu.current_screen(),
            MenuScreen {
                title: "Item 3",
                items: vec!["Item 3 1", "Item 3 2", "Item 3 3", "Item 3 4"],
                selected_item: 0,
                active_setting: None,
            }
        );
    }

    #[test]
    fn moving_only_item_to_end_has_no_effect() {
        let mut menu = create_sublevel_menu("x", 1, false);
        menu.move_item_to_end(&["Item 1"]);

        assert_eq!(
            menu.current_screen(),
            MenuScreen {
                title: "x",
                items: vec!["Item 1"],
                selected_item: 0,
                active_setting: None,
            }
        );
    }

    #[test]
    fn choosing_a_string_setting_requests_a_dialog() {
        let mut menu = Menu::new("My Menu");
        let setting = MenuItemType::Setting(MenuSettingState::player_name(
            "Special Power",
            "Special Power",
            "sp",
            "none",
            0,
        ));
        let setting_item = MenuItem::new(
            "Special Power".to_owned(),
            "Special Power: none".to_owned(),
            setting,
        );
        menu.append_item(&[], setting_item);

        menu.choose();

        assert_eq!(
            menu.current_screen(),
            MenuScreen {
                title: "My Menu",
                items: vec!["Special Power: none"],
                selected_item: 0,
                active_setting: Some(MenuSettingState::player_name(
                    "Special Power",
                    "Special Power",
                    "sp",
                    "none",
                    0,
                )),
            }
        );
    }

    fn create_sublevel_menu(
        title: &str,
        num_items: usize,
        second_level: bool,
    ) -> Menu {
        let mut menu = Menu::new(title);
        for i in 1..=num_items {
            let name = format!("Item {}", i);
            menu.append_sublevel(&[], &name, &name);
            if second_level {
                for j in 1..=num_items {
                    menu.append_sublevel(
                        &[&name],
                        &format!("Item {} {}", i, j),
                        &format!("Item {} {}", i, j),
                    );
                }
            }
        }
        menu
    }
}
