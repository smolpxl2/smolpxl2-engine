#[derive(Clone)]
pub enum GameMode {
    Title(usize),
    Menu,
    InGame,
}
