use std::{cell::RefCell, rc::Rc};

use js_sys::Function;
use wasm_bindgen::{Clamped, JsValue};
use web_sys::{CanvasRenderingContext2d, ImageData};

use crate::color::Color;
use crate::evt::Evt;
use crate::game::Game;
use crate::game_state::custom_setting::CustomSettingValue;
use crate::game_state::game_mode::GameMode;
use crate::game_state::game_settings::GameSettings;
use crate::game_state::gs::GameState;
use crate::game_state::menu::{
    MenuAction, MenuItem, MenuItemType, MenuLevel, MenuSettingState,
    MenuSettingType,
};
use crate::input::Input;
use crate::input_mapper::InputMapper;
use crate::js_conversion::{
    array_of_array_of_string, array_of_color, array_of_str, array_of_string,
};
use crate::screen_creator::ScreenCreator;
use crate::title_page::{TextTitlePage, TitlePage};
use crate::Smolpxl2Ui;

#[allow(dead_code)]
pub trait Ui {
    fn set_screen_size(&self, width: u32, height: u32);
    fn show_smolpxl_bar(&self);
    fn set_render(&self, render_fn: &Function);
    fn set_frame(&self, frame_fn: &Function);
    fn request_render(&self);
    fn start_frames(&self);
    fn set_event_handler(&self, event_handler_fn: &Function);
    fn set_controls_mode(&self, mode: &str);
    fn set_controls(
        &self,
        global_controls: &[String],
        player_controls: &[String],
        num_players: u32,
        player_colors: &[Color],
    );
    fn set_title(&self, items: JsValue);
    fn set_menu(&self, title: &str, selected: usize, items: JsValue);
    fn prompt(&self, message: &str, default_value: &str) -> Option<String>;
}

impl Ui for Smolpxl2Ui {
    fn set_screen_size(&self, width: u32, height: u32) {
        Smolpxl2Ui::set_screen_size(&self, width, height)
    }

    fn show_smolpxl_bar(&self) {
        Smolpxl2Ui::show_smolpxl_bar(&self)
    }

    fn set_render(&self, render_fn: &Function) {
        Smolpxl2Ui::set_render(&self, render_fn)
    }

    fn set_frame(&self, frame_fn: &Function) {
        Smolpxl2Ui::set_frame(&self, frame_fn)
    }

    fn request_render(&self) {
        Smolpxl2Ui::request_render(&self)
    }

    fn start_frames(&self) {
        Smolpxl2Ui::start_frames(&self)
    }

    fn set_event_handler(&self, event_handler_fn: &Function) {
        Smolpxl2Ui::set_event_handler(&self, event_handler_fn)
    }

    fn set_controls_mode(&self, mode: &str) {
        Smolpxl2Ui::set_controls_mode(&self, mode);
    }

    fn set_controls(
        &self,
        global_controls: &[String],
        player_controls: &[String],
        num_players: u32,
        player_colors: &[Color],
    ) {
        Smolpxl2Ui::set_controls(
            &self,
            array_of_string(global_controls),
            array_of_string(player_controls),
            num_players,
            array_of_color(player_colors),
        )
    }

    fn set_title(&self, items: JsValue) {
        Smolpxl2Ui::set_title(&self, items)
    }

    fn set_menu(&self, title: &str, selected: usize, items: JsValue) {
        Smolpxl2Ui::set_menu(&self, title, selected, items)
    }

    fn prompt(&self, message: &str, default_value: &str) -> Option<String> {
        Smolpxl2Ui::prompt(&self, message, default_value)
    }
}

const MAX_CATCHUP_FRAMES: u8 = 10;

pub struct Engine<U: Ui = Smolpxl2Ui> {
    game: Rc<RefCell<dyn Game>>,
    game_state: GameState,
    model: Option<serde_json::Value>,
    input_mapper: InputMapper,
    ui: U,
    last_frame_ts: Option<f64>,
}

impl<U: Ui> Engine<U> {
    pub fn new(ui: U, game: Rc<RefCell<dyn Game>>) -> Self {
        let mut game_state = GameState::default();
        let model = game.borrow().initial_model();
        game_state.inputs.push(Input::EngineStarting);

        let mut ret = Self {
            ui,
            game,
            game_state,
            model: Some(model),
            input_mapper: Default::default(),
            last_frame_ts: None,
        };

        // Set the menu as edited so we add default items in update
        ret.game_state.menu.edit();
        // Set the player settings as edited so we add new players in update
        ret.game_state.player_settings.edit();

        // Perform the requested updates
        ret.update();

        ret
    }

    pub fn frame(
        &mut self,
        ts: f64,
        canvas_rendering_context2d: CanvasRenderingContext2d,
        width: u32,
        height: u32,
    ) {
        let frame_time_ms =
            self.game_state.game_settings.view().frame_time_ms as f64;

        let mut sim_ts = self.last_frame_ts.unwrap_or(ts - frame_time_ms);
        let mut catchups = 0;

        // Catch up our simulation to the current time (but limit this
        // to a reasonable number - we allow the simulation to slow down
        // if things are really slow.)
        while sim_ts < ts {
            self.update();
            catchups += 1;
            if catchups < MAX_CATCHUP_FRAMES {
                sim_ts += frame_time_ms;
            } else {
                sim_ts = ts;
                break;
            }
        }
        self.last_frame_ts = Some(sim_ts);

        // If we did any simulation, render the new model.
        if catchups > 0 {
            self.render(canvas_rendering_context2d, width, height);
        }
    }

    pub fn render(
        &mut self,
        canvas_rendering_context2d: CanvasRenderingContext2d,
        width: u32,
        height: u32,
    ) {
        let screen_creator = ScreenCreator::new(width, height);
        let screen = self.game.borrow().view(
            screen_creator,
            &self.game_state,
            self.model.as_ref().unwrap(),
        );

        if let Some(screen) = screen {
            let image_data = ImageData::new_with_u8_clamped_array(
                Clamped(screen.pixels()),
                width,
            )
            .expect("Failed to create ImageData");

            canvas_rendering_context2d
                .put_image_data(&image_data, 0.0, 0.0)
                .expect("Failed to put image data");
        }
    }

    pub fn on_event(&mut self, evt: Evt) {
        match evt {
            Evt::ShowControlsClicked => self.ui.set_controls_mode("overlay"),
            Evt::PlayerButtonDown { player, button } => self
                .on_input(self.input_mapper.player_button_down(player, button)),
            Evt::PlayerButtonUp { player, button } => self
                .on_input(self.input_mapper.player_button_up(player, button)),
            Evt::GeneralButtonDown { button } => {
                self.on_input(self.input_mapper.general_button_down(button))
            }
            Evt::GeneralButtonUp { button } => {
                self.on_input(self.input_mapper.general_button_up(button))
            }
            Evt::KeyDown { key } => {
                self.on_input(self.input_mapper.key_down(key))
            }
            Evt::KeyUp { key } => self.on_input(self.input_mapper.key_up(key)),
        }
    }

    pub fn ui(&self) -> &U {
        &self.ui
    }

    fn update(&mut self) {
        self.model = Some(
            self.game
                .borrow_mut()
                .update(&mut self.game_state, self.model.take().unwrap()),
        );
        if self.game_state.name.reset_edited() {
            // TODO: set page title?
        }
        if self.game_state.game_mode.reset_edited() {
            self.switch_mode();
        }
        if self.game_state.player_settings.reset_edited()
            || self.game_state.general_controls_settings.reset_edited()
        {
            // Make sure there are enough players
            let min_players =
                self.game_state.player_settings.view().min_players;
            while self.game_state.player_settings.view().player_details.len()
                < min_players
            {
                self.game_state.add_player();
            }

            // Set up the controls
            let player_settings = self.game_state.player_settings.view();
            let general_controls_settings =
                self.game_state.general_controls_settings.view();
            let player_inputs = player_settings.inputs.to_ui_strings();
            let player_colors: Vec<Color> = player_settings
                .player_details
                .iter()
                .map(|dets| {
                    dets.color.clone().unwrap_or(Color::new(255, 255, 255))
                })
                .collect();
            self.ui.set_controls(
                general_controls_settings,
                &player_inputs,
                player_settings.player_details.len() as u32,
                &player_colors,
            );

            // We modified the player_settings, so reset again
            self.game_state.player_settings.reset_edited();
        }
        if self.game_state.game_settings.reset_edited() {
            self.setup_screen(self.game_state.game_settings.view());
        }
        self.game_state.title_pages.reset_edited();

        if self.game_state.menu.reset_edited() {
            self.ensure_default_menu_items();
        }
    }

    fn setup_screen(&self, game_settings: &GameSettings) {
        self.ui.set_screen_size(
            game_settings.size.width,
            game_settings.size.height,
        );
        if game_settings.smolpxl_bar {
            self.ui.show_smolpxl_bar();
        }
        self.ui
            .set_controls_mode(game_settings.default_controls_mode.as_str());
    }

    fn ensure_default_menu_items(&mut self) {
        let player_settings = self.game_state.player_settings.view();
        let player_details = &player_settings.player_details;

        let mut players_level = MenuLevel::new("Players".to_owned());
        for (i, player) in player_details.iter().enumerate() {
            let id = format!("player_{i}");
            let name = player
                .name
                .as_ref()
                .cloned()
                .unwrap_or_else(|| format!("Player {i}"));

            let mut this_player_level = MenuLevel::new(name.clone());

            let name_setting =
                MenuSettingState::player_name("Name", &name, "Name", &name, i);

            this_player_level.items.push(name_setting.into_menu_item());

            for custom_setting in &player_settings.custom_settings {
                let setting = MenuSettingState::player_custom_setting(
                    &custom_setting.display_name,
                    &custom_setting.display_name,
                    &custom_setting.name,
                    player
                        .custom_values
                        .get(&custom_setting.name, &custom_setting.typ),
                    i,
                );
                this_player_level.items.push(setting.into_menu_item());
            }

            let back_item = MenuItem::new(
                "Back".to_owned(),
                "Back".to_owned(),
                MenuItemType::Action(MenuAction::MenuOut),
            );
            this_player_level.items.push(back_item);

            let this_player_item = MenuItem::new(
                id,
                name,
                MenuItemType::Sublevel(this_player_level),
            );
            players_level.items.push(this_player_item);
        }

        if player_details.len() < player_settings.max_players {
            let add_item = MenuItem::new(
                "Add player".to_owned(),
                "Add player".to_owned(),
                MenuItemType::Action(MenuAction::AddPlayer),
            );
            players_level.items.push(add_item);
        }

        let back_item = MenuItem::new(
            "Back".to_owned(),
            "Back".to_owned(),
            MenuItemType::Action(MenuAction::MenuOut),
        );
        players_level.items.push(back_item);

        let players_item = MenuItem::new(
            "Players".to_owned(),
            "Players".to_owned(),
            MenuItemType::Sublevel(players_level),
        );

        let menu = self.game_state.menu.edit();
        if let Some(existing) = menu.get_item_mut(&["Players"]) {
            *existing = players_item;
        } else {
            menu.append_item(&[], players_item);
        }

        let return_item = MenuItem::new(
            "Back to title screen".to_owned(),
            "Back to title screen".to_owned(),
            MenuItemType::Action(MenuAction::BackToTitleScreen),
        );
        if let Some(existing) = menu.get_item_mut(&["Back to title screen"]) {
            *existing = return_item;
        } else {
            menu.append_item(&[], return_item);
        }

        self.game_state.menu.reset_edited();
    }

    fn on_input(&mut self, input: Input) {
        match self.game_state.game_mode.view() {
            GameMode::InGame => self.game_state.inputs.push(input),
            GameMode::Menu => self.on_input_menu(input),
            GameMode::Title(_) => self.on_input_title(input),
        }
    }

    fn on_input_menu(&mut self, input: Input) {
        match input {
            Input::GeneralInputDown { button } => match button.as_str() {
                "Menu" => self.menu_out(),
                "Start" => self.menu_choose(),
                b => panic!("Unknown general button {b}"),
            },
            Input::PlayerInputDown { player: _, button } => {
                match button.as_str() {
                    "ArrowUp" => self.menu_prev(),
                    "ArrowDown" => self.menu_next(),
                    "ButtonA" => self.menu_choose(),
                    _ => {}
                }
            }
            _ => (),
        }
    }

    fn on_input_title(&mut self, input: Input) {
        match input {
            Input::GeneralInputDown { button } => match button.as_str() {
                "Start" => self.start_game(),
                "Menu" => self.launch_menu(),
                _ => {}
            },
            Input::PlayerInputDown { player: _, button }
                if button == "ButtonA" =>
            {
                self.start_game()
            }
            _ => (),
        }
    }

    fn start_game(&mut self) {
        // Set game mode
        *self.game_state.game_mode.edit() = GameMode::InGame;
        self.game_state.game_mode.reset_edited();

        // Tell the game we're starting
        self.game_state.inputs.push(Input::GameStarting);

        // Clear the menu
        self.ui.set_title(array_of_array_of_string(&Vec::new()));

        self.switch_mode();
    }

    fn menu_next(&mut self) {
        self.game_state.menu.edit().move_next();
        self.refresh_menu();
    }

    fn menu_prev(&mut self) {
        self.game_state.menu.edit().move_prev();
        self.refresh_menu();
    }

    fn menu_choose(&mut self) {
        let action = self.game_state.menu.edit().choose();
        match action {
            Some(MenuAction::BackToTitleScreen) => {
                *self.game_state.game_mode.edit() = GameMode::Title(0);
                self.game_state.game_mode.reset_edited();
            }
            Some(MenuAction::AddPlayer) => {
                self.game_state.add_player();
                self.ensure_default_menu_items();
                self.refresh_menu();
            }
            Some(MenuAction::MenuOut) | None => {
                // Handled by menu code - we just need to refresh
                self.refresh_menu();
            }
        }
        self.switch_mode();
    }

    fn menu_out(&mut self) {
        if self.game_state.menu.view().is_at_top() {
            self.close_menu();
        } else {
            self.game_state.menu.edit().move_out();
            self.game_state.menu.reset_edited();
            self.refresh_menu();
        }
    }

    fn close_menu(&mut self) {
        // Set game mode to pre_menu_game_mode
        *self.game_state.game_mode.edit() =
            self.game_state.pre_menu_game_mode.clone();
        self.game_state.game_mode.reset_edited();

        // Go back to the top level of the menu
        self.game_state.menu.edit().move_top();
        self.game_state.menu.reset_edited();

        // Clear menu items off the screen
        self.ui.set_menu("", 0, array_of_string(&[]));

        self.switch_mode();
    }

    fn launch_menu(&mut self) {
        // Set game mode
        self.game_state.pre_menu_game_mode =
            self.game_state.game_mode.view().clone();

        *self.game_state.game_mode.edit() = GameMode::Menu;
        self.game_state.game_mode.reset_edited();

        self.refresh_menu();
        self.switch_mode();
    }

    fn refresh_menu(&mut self) {
        let menu_screen = self.game_state.menu.view().current_screen();

        self.ui.set_menu(
            menu_screen.title,
            menu_screen.selected_item,
            array_of_str(&menu_screen.items),
        );
        if let Some(active_setting) = menu_screen.active_setting {
            match &active_setting.value {
                CustomSettingValue::String(value) => {
                    let response =
                        self.ui.prompt(&active_setting.description, value);
                    self.handle_menu_dialog_response(active_setting, response);
                }
            }
        }
    }

    fn handle_menu_dialog_response(
        &mut self,
        setting: MenuSettingState,
        response: Option<String>,
    ) {
        self.game_state.menu.edit().dialog_done();

        if let Some(response) = response {
            match setting.typ {
                MenuSettingType::PlayerName { player_num } => {
                    self.game_state.player_settings.edit().player_details
                        [player_num]
                        .name = Some(response);

                    self.ensure_default_menu_items();
                    self.refresh_menu();
                }
                MenuSettingType::PlayerCustomSetting { player_num, .. } => {
                    self.game_state.player_settings.edit().player_details
                        [player_num]
                        .custom_values
                        .insert(
                            &setting.key,
                            CustomSettingValue::String(response),
                        );

                    self.ensure_default_menu_items();
                    self.refresh_menu();
                }
            }
        }
    }

    pub fn switch_mode(&mut self) {
        match self.game_state.game_mode.view() {
            GameMode::Title(page_number) => self.draw_title_page(*page_number),
            GameMode::Menu => {}
            GameMode::InGame => self.ui.start_frames(),
        };
    }

    fn draw_title_page(&mut self, page_number: usize) {
        self.ensure_title_page_exists();
        let title_pages = self.game_state.title_pages.view();
        let page_number = page_number % title_pages.len();
        let page = &title_pages[page_number];

        self.ui.set_title(array_of_array_of_string(page.to_menu()));
    }

    fn ensure_title_page_exists(&mut self) {
        let title_pages = self.game_state.title_pages.view();
        if title_pages.is_empty() {
            let title_page = self.default_title_page();
            let title_pages = self.game_state.title_pages.edit();
            title_pages.push(title_page);
            self.game_state.title_pages.reset_edited();
        }
    }

    fn default_title_page(&self) -> TitlePage {
        TitlePage::Text(TextTitlePage::new_column(vec![
            if let Some(n) = self.game_state.name.view() {
                n.clone()
            } else {
                "Smolpxl2 Game".to_owned()
            },
            "".to_owned(),
            "Press Enter to start".to_owned(),
            "Or Esc for menu".to_owned(),
        ]))
    }
}

#[cfg(test)]
mod test {
    use std::{cell::RefCell, rc::Rc};

    use wasm_bindgen::JsValue;
    use web_sys::CanvasRenderingContext2d;

    use crate::{
        color::Color,
        game::Game,
        game_state::{
            custom_setting::{CustomSetting, CustomSettingValue},
            gs::GameState,
            menu::{MenuAction, MenuItemType, MenuSettingType},
            player_input::PlayerInput,
        },
        screen::Screen,
        screen_creator::ScreenCreator,
    };

    use super::{Engine, Ui};

    #[test]
    fn initial_game_has_basic_controls() {
        // Given an engine
        let ui = FakeUi {};
        let game = Rc::new(RefCell::new(FakeGame::new()));
        let engine = Engine::new(ui, game);

        // Then after we just made it,
        // It has the default (normal) set of controls
        let player_settings = engine.game_state.player_settings.view();
        let inputs = player_settings.inputs.as_vec();
        assert_eq!(inputs.len(), 5);
        assert!(inputs.contains(&PlayerInput::ArrowUp));
        assert!(inputs.contains(&PlayerInput::ArrowDown));
        assert!(inputs.contains(&PlayerInput::ArrowLeft));
        assert!(inputs.contains(&PlayerInput::ArrowRight));
        assert!(inputs.contains(&PlayerInput::ButtonA));
    }

    #[test]
    fn default_menu_items_are_created_for_one_player() {
        let ui = FakeUi {};
        let game = Rc::new(RefCell::new(FakeGame::new()));
        let engine = Engine::new(ui, game);

        // Sanity: there is 1 player
        let player_settings = engine.game_state.player_settings.view();
        assert_eq!(player_settings.min_players, 1);
        assert_eq!(player_settings.max_players, 8);
        assert_eq!(player_settings.player_details.len(), 1);

        // We created a submenu for the player
        let menu = engine.game_state.menu.view();
        let selected_item = menu.selected_item();
        assert_eq!(selected_item.name, "Players");
        assert_eq!(selected_item.display_name, "Players");

        let MenuItemType::Sublevel(players) = &selected_item.typ else {
            panic!("selected_item.typ={:?}", selected_item.typ);
        };

        assert_eq!(players.display_name, "Players");
        assert_eq!(players.items.len(), 3);

        let p0 = &players.items[0];
        assert_eq!(p0.name, "player_0");
        assert_eq!(p0.display_name, "Player 0");

        let MenuItemType::Sublevel(p0_sublevel) = &p0.typ else {
            panic!("item0.typ={:?}", p0.typ);
        };
        assert_eq!(p0_sublevel.display_name, "Player 0");
        assert_eq!(p0_sublevel.items.len(), 2);

        let p0name = &p0_sublevel.items[0];
        assert_eq!(p0name.name, "Name");
        assert_eq!(p0name.display_name, "Name: Player 0");

        let MenuItemType::Setting(p0setting) = &p0name.typ else {
            panic!("p0name.typ={:?}", p0name.typ);
        };
        assert_eq!(p0setting.key, "Name");
        assert_eq!(p0setting.description, "Name");
        assert_eq!(p0setting.display_name, "Player 0");
        assert_eq!(
            p0setting.value,
            CustomSettingValue::String("Player 0".to_owned())
        );

        let p0back = &p0_sublevel.items[1];
        assert_eq!(p0back.name, "Back");
        assert_eq!(p0back.display_name, "Back");
        assert_eq!(p0back.typ, MenuItemType::Action(MenuAction::MenuOut));

        let players_add = &players.items[1];
        assert_eq!(players_add.name, "Add player");
        assert_eq!(players_add.display_name, "Add player");
        assert_eq!(
            players_add.typ,
            MenuItemType::Action(MenuAction::AddPlayer)
        );

        let players_back = &players.items[2];
        assert_eq!(players_back.name, "Back");
        assert_eq!(players_back.display_name, "Back");
        assert_eq!(players_back.typ, MenuItemType::Action(MenuAction::MenuOut));

        let back_to_title = menu.get_item(&["Back to title screen"]).unwrap();
        assert_eq!(back_to_title.name, "Back to title screen");
        assert_eq!(back_to_title.display_name, "Back to title screen");
        assert_eq!(
            back_to_title.typ,
            MenuItemType::Action(MenuAction::BackToTitleScreen)
        );

        // The menu is not marked as edited
        assert!(!engine.game_state.menu.is_edited());
    }

    #[test]
    fn default_menu_items_are_created_for_multiple_players() {
        let ui = FakeUi {};
        let game = Rc::new(RefCell::new(FakeGame::new()));
        let mut engine = Engine::new(ui, game);
        let player_settings = engine.game_state.player_settings.edit();
        player_settings.max_players = 2;

        // Set up 2 players with custom names
        add_players(&mut engine, 2);

        engine.ensure_default_menu_items();

        // We created a submenu for each player
        let menu = engine.game_state.menu.view();
        let selected_item = menu.selected_item();
        assert_eq!(selected_item.name, "Players");
        assert_eq!(selected_item.display_name, "Players");

        let MenuItemType::Sublevel(players) = &selected_item.typ else {
            panic!("selected_item.typ={:?}", selected_item.typ);
        };

        // Since 2 players is the max, there is no "Add player" button
        assert_eq!(players.display_name, "Players");
        assert_eq!(players.items.len(), 3);

        let p0 = &players.items[0];
        assert_eq!(p0.name, "player_0");
        assert_eq!(p0.display_name, "MyPlayer 1");

        let MenuItemType::Sublevel(p0_sublevel) = &p0.typ else {
            panic!("p0.typ={:?}", p0.typ);
        };
        assert_eq!(p0_sublevel.display_name, "MyPlayer 1");
        assert_eq!(p0_sublevel.items.len(), 2);

        let p0name = &p0_sublevel.items[0];
        assert_eq!(p0name.name, "Name");
        assert_eq!(p0name.display_name, "Name: MyPlayer 1");

        let MenuItemType::Setting(p0setting) = &p0name.typ else {
            panic!("p0name.typ={:?}", p0name.typ);
        };
        assert_eq!(p0setting.key, "Name");
        assert_eq!(p0setting.description, "Name");
        assert_eq!(p0setting.display_name, "MyPlayer 1");
        assert_eq!(
            p0setting.value,
            CustomSettingValue::String("MyPlayer 1".to_owned())
        );

        let p0back = &p0_sublevel.items[1];
        assert_eq!(p0back.name, "Back");
        assert_eq!(p0back.display_name, "Back");
        assert_eq!(p0back.typ, MenuItemType::Action(MenuAction::MenuOut));

        let p1 = &players.items[1];
        assert_eq!(p1.name, "player_1");
        assert_eq!(p1.display_name, "MyPlayer 2");

        let MenuItemType::Sublevel(p1_sublevel) = &p1.typ else {
            panic!("p1.typ={:?}", p1.typ);
        };
        assert_eq!(p1_sublevel.display_name, "MyPlayer 2");
        assert_eq!(p1_sublevel.items.len(), 2);

        let p1name = &p1_sublevel.items[0];
        assert_eq!(p0name.name, "Name");
        assert_eq!(p1name.display_name, "Name: MyPlayer 2");

        let MenuItemType::Setting(p1setting) = &p1name.typ else {
            panic!("p1name.typ={:?}", p1name.typ);
        };
        assert_eq!(p1setting.key, "Name");
        assert_eq!(p1setting.description, "Name");
        assert_eq!(p1setting.display_name, "MyPlayer 2");
        assert_eq!(
            p1setting.value,
            CustomSettingValue::String("MyPlayer 2".to_owned())
        );

        let p1back = &p1_sublevel.items[1];
        assert_eq!(p1back.name, "Back");
        assert_eq!(p1back.display_name, "Back");
        assert_eq!(p1back.typ, MenuItemType::Action(MenuAction::MenuOut));

        let players_back = &players.items[2];
        assert_eq!(players_back.name, "Back");
        assert_eq!(players_back.display_name, "Back");
        assert_eq!(players_back.typ, MenuItemType::Action(MenuAction::MenuOut));

        // The menu is not marked as edited
        assert!(!engine.game_state.menu.is_edited());
    }

    #[test]
    fn custom_settings_create_new_menu_items() {
        let ui = FakeUi {};
        let game = Rc::new(RefCell::new(FakeGame::new()));
        let mut engine = Engine::new(ui, game);

        // Set up 2 players and add a custom setting
        add_players(&mut engine, 2);
        {
            engine
                .game_state
                .player_settings
                .edit()
                .custom_settings
                .push(CustomSetting::new("food", "Food"));
        }

        // This is what we are testing
        engine.ensure_default_menu_items();

        // Each player menu has our custom setting
        assert_is_player_custom_setting(
            &mut engine,
            &["Players", "player_0", "food"],
            "Food",
            0,
        );
        assert_is_player_custom_setting(
            &mut engine,
            &["Players", "player_1", "food"],
            "Food",
            1,
        );
    }

    #[test]
    fn custom_settings_values_appear_in_the_menu() {
        let ui = FakeUi {};
        let game = Rc::new(RefCell::new(FakeGame::new()));
        let mut engine = Engine::new(ui, game);

        // Set up 2 players and add a custom setting
        add_players(&mut engine, 2);
        {
            let player_settings = engine.game_state.player_settings.edit();

            player_settings
                .custom_settings
                .push(CustomSetting::new("food", "Food"));

            // And set a value on the custom settings
            player_settings.player_details[0].custom_values.insert(
                "food",
                CustomSettingValue::String("Chips and beans".to_owned()),
            );
            player_settings.player_details[1].custom_values.insert(
                "food",
                CustomSettingValue::String(
                    "Egg on toast followed by coffee cake on a slab of \
                    marble cooled in the cellar"
                        .to_owned(),
                ),
            );
        }

        // This is what we are testing
        engine.ensure_default_menu_items();

        // Each player menu has our custom setting, with its value
        assert_is_player_custom_setting(
            &mut engine,
            &["Players", "player_0", "food"],
            "Food: Chips and beans",
            0,
        );
        assert_is_player_custom_setting(
            &mut engine,
            &["Players", "player_1", "food"],
            "Food: Egg on toast fo\u{2026}",
            1,
        );
    }

    #[test]
    fn frame_calls_update_and_view() {
        // Given an engine
        let ui = FakeUi {};
        let game = Rc::new(RefCell::new(FakeGame::new()));
        let mut engine = Engine::new(ui, game.clone());
        let context = CanvasRenderingContext2d::from(JsValue::null());

        // Forget any update() calls from Engine::new
        game.borrow_mut().clear_tracking();

        // When I call frame
        engine.frame(12.0, context, 20, 10);

        // Then the game's update and view methods were called
        assert_eq!(game.borrow().update_calls.borrow().len(), 1);
        assert_eq!(game.borrow().view_calls.borrow().len(), 1);
    }

    #[test]
    fn calling_frame_very_soon_after_last_time_does_not_call_update_or_view() {
        // Given an engine with fps = 10
        let ui = FakeUi {};
        let game = Rc::new(RefCell::new(FakeGame::new()));
        let mut engine = Engine::new(ui, game.clone());
        engine.game_state.game_settings.edit().set_fps(10);
        let context = || CanvasRenderingContext2d::from(JsValue::null());

        // Forget any update() calls from Engine::new
        game.borrow_mut().clear_tracking();

        // When I call frame once, then again to trigger next frame, then
        // again within a frame time
        engine.frame(12.0, context(), 20, 10);
        engine.frame(13.0, context(), 20, 10);
        engine.frame(14.0, context(), 20, 10);

        // Then the game's update and view methods were only called twice
        assert_eq!(game.borrow().update_calls.borrow().len(), 2);
        assert_eq!(game.borrow().view_calls.borrow().len(), 2);
    }

    #[test]
    fn calling_frame_lots_of_times_close_together_eventually_calls_update() {
        // Given an engine with fps = 10 and ts set
        let ui = FakeUi {};
        let game = Rc::new(RefCell::new(FakeGame::new()));
        let mut engine = Engine::new(ui, game.clone());
        engine.game_state.game_settings.edit().set_fps(10);
        let context = || CanvasRenderingContext2d::from(JsValue::null());
        engine.frame(99.0, context(), 20, 10);
        game.borrow_mut().clear_tracking();

        // When I call frame  lots of times with small ts updates
        for i in 0..100 {
            engine.frame(100.0 + i as f64, context(), 20, 10);
        }

        // Then the game's update and view methods were eventually called
        assert_eq!(game.borrow().update_calls.borrow().len(), 1);
        assert_eq!(game.borrow().view_calls.borrow().len(), 1);
    }

    #[test]
    fn calling_frame_after_a_slowdown_catches_up_some_updates() {
        // Given an engine with fps = 60 and ts set
        let ui = FakeUi {};
        let game = Rc::new(RefCell::new(FakeGame::new()));
        let mut engine = Engine::new(ui, game.clone());
        engine.game_state.game_settings.edit().set_fps(60);
        let context = || CanvasRenderingContext2d::from(JsValue::null());
        engine.frame(11.0, context(), 20, 10);
        game.borrow_mut().clear_tracking();

        // When I call frame again 4 frames later
        engine.frame(11.0 + (1000.0 / 60.0) * 4.0, context(), 20, 10);

        // Then the game's update method was called 5 times
        assert_eq!(game.borrow().update_calls.borrow().len(), 5);
        // But view was still only called once
        assert_eq!(game.borrow().view_calls.borrow().len(), 1);
    }

    #[test]
    fn catchup_after_slowdown_is_limited_to_10_frames() {
        // Given an engine with fps = 30 and ts set
        let ui = FakeUi {};
        let game = Rc::new(RefCell::new(FakeGame::new()));
        let mut engine = Engine::new(ui, game.clone());
        engine.game_state.game_settings.edit().set_fps(30);
        let context = || CanvasRenderingContext2d::from(JsValue::null());
        engine.frame(11.0, context(), 20, 10);
        game.borrow_mut().clear_tracking();

        // When I call frame, then call it again 15 frames later
        engine.frame(11.0 + (1000.0 / 30.0) * 15.0, context(), 20, 10);

        // Then the game's update method was only called 10 times
        assert_eq!(game.borrow().update_calls.borrow().len(), 10);
        assert_eq!(game.borrow().view_calls.borrow().len(), 1);
    }

    struct FakeUi {}

    impl Ui for FakeUi {
        fn set_screen_size(&self, _width: u32, _height: u32) {}

        fn show_smolpxl_bar(&self) {}

        fn set_render(&self, _render_fn: &js_sys::Function) {}

        fn set_frame(&self, _frame_fn: &js_sys::Function) {}

        fn request_render(&self) {}

        fn start_frames(&self) {}

        fn set_event_handler(&self, _event_handler_fn: &js_sys::Function) {}

        fn set_controls_mode(&self, _mode: &str) {}

        fn set_controls(
            &self,
            _global_controls: &[String],
            _player_controls: &[String],
            _num_players: u32,
            _player_colors: &[Color],
        ) {
        }

        fn set_title(&self, _items: JsValue) {}

        fn set_menu(&self, _title: &str, _selected: usize, _items: JsValue) {}

        fn prompt(
            &self,
            _message: &str,
            _default_value: &str,
        ) -> Option<String> {
            None
        }
    }

    fn assert_is_player_custom_setting(
        engine: &mut Engine<FakeUi>,
        location: &[&str],
        display_name: &str,
        index: usize,
    ) {
        let menu = engine.game_state.menu.edit();
        let item = menu.get_item_mut(location).unwrap();

        assert_eq!(item.display_name, display_name);

        let MenuItemType::Setting(setting) = &item.typ else {
            panic!("item was not a Setting!")
        };

        let MenuSettingType::PlayerCustomSetting { player_num } = &setting.typ
        else {
            panic!("item settings was not a custom setting")
        };

        assert_eq!(*player_num, index);
    }

    fn add_players(engine: &mut Engine<FakeUi>, num_players: usize) {
        while engine
            .game_state
            .player_settings
            .view()
            .player_details
            .len()
            < num_players
        {
            engine.game_state.add_player();
        }

        let player_settings = engine.game_state.player_settings.edit();
        for i in 0..num_players {
            player_settings.player_details[i].name =
                Some(format!("MyPlayer {}", i + 1));
        }
    }

    struct FakeGame {
        view_calls: RefCell<Vec<serde_json::Value>>,
        update_calls: RefCell<Vec<serde_json::Value>>,
    }

    impl FakeGame {
        fn new() -> Self {
            Self {
                view_calls: RefCell::new(Vec::new()),
                update_calls: RefCell::new(Vec::new()),
            }
        }

        fn clear_tracking(&self) {
            self.view_calls.borrow_mut().clear();
            self.update_calls.borrow_mut().clear();
        }
    }

    impl Game for FakeGame {
        fn initial_model(&self) -> serde_json::Value {
            serde_json::Value::Null
        }

        fn view(
            &self,
            _screen_creator: ScreenCreator,
            _game_state: &GameState,
            model_json: &serde_json::Value,
        ) -> Option<Screen> {
            self.view_calls.borrow_mut().push(model_json.clone());
            None
        }

        fn update(
            &mut self,
            _game_state: &mut GameState,
            model_json: serde_json::Value,
        ) -> serde_json::Value {
            self.update_calls.borrow_mut().push(model_json.clone());
            serde_json::Value::Null
        }
    }
}
