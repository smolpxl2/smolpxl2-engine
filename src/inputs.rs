use std::collections::VecDeque;

use crate::input::Input;

#[derive(Debug, Default)]
pub struct Inputs {
    inner: VecDeque<Input>,
}

impl Inputs {
    pub fn push(&mut self, input: Input) {
        // TODO: limit size?
        // TODO: filter based on what we're interested in
        self.inner.push_back(input);
    }

    pub fn extend(&mut self, inputs: impl IntoIterator<Item = Input>) {
        self.inner.extend(inputs.into_iter())
    }

    pub fn take_all(&mut self) -> Vec<Input> {
        self.inner.drain(..).collect()
    }

    pub fn last(&self) -> Option<&Input> {
        self.inner.back()
    }
}
