pub mod color;
mod decode_image;
mod engine;
mod evt;
mod example;
mod example2;
mod game;
pub mod game_state;
mod input;
pub mod input_filter;
mod input_mapper;
pub mod input_processor;
mod inputs;
mod js_conversion;
mod latest_n;
pub mod repeat_remover;
mod screen;
pub mod screen_creator;
mod title_page;

use std::{cell::RefCell, rc::Rc};

use evt::Evt;
use example::reflower_game::ReflowerGame;
use js_sys::Function;
use wasm_bindgen::prelude::*;
use web_sys::CanvasRenderingContext2d;

use crate::engine::Engine;
use crate::example2::snake_game::SnakeGame;
use crate::game::Game;

#[wasm_bindgen(module = "/lib/smolpxl2-ui/smolpxl2-ui.js")]
extern "C" {
    pub type Smolpxl2Ui;

    #[wasm_bindgen(method)]
    fn set_screen_size(this: &Smolpxl2Ui, width: u32, height: u32);

    #[wasm_bindgen(method)]
    fn show_smolpxl_bar(this: &Smolpxl2Ui);

    #[wasm_bindgen(method)]
    fn set_render(this: &Smolpxl2Ui, render_fn: &Function);

    #[wasm_bindgen(method)]
    fn set_frame(this: &Smolpxl2Ui, frame_fn: &Function);

    #[wasm_bindgen(method)]
    fn request_render(this: &Smolpxl2Ui);

    #[wasm_bindgen(method)]
    fn start_frames(this: &Smolpxl2Ui);

    #[wasm_bindgen(method)]
    fn set_event_handler(this: &Smolpxl2Ui, event_handler_fn: &Function);

    // TODO: wrap so we can use an enum
    #[wasm_bindgen(method)]
    fn set_controls_mode(this: &Smolpxl2Ui, mode: &str);

    #[wasm_bindgen(method)]
    fn set_controls(
        this: &Smolpxl2Ui,
        global_controls: JsValue, // TODO: wrap so we can pass &[&str]
        player_controls: JsValue,
        num_players: u32,
        player_colors: JsValue,
    );

    #[wasm_bindgen(method)]
    fn set_title(this: &Smolpxl2Ui, items: JsValue);

    #[wasm_bindgen(method)]
    fn set_menu(
        this: &Smolpxl2Ui,
        title: &str,
        selected: usize,
        items: JsValue,
    );

    #[wasm_bindgen(method)]
    fn prompt(
        this: &Smolpxl2Ui,
        message: &str,
        defaultValue: &str,
    ) -> Option<String>;
}

#[wasm_bindgen]
pub struct Smolpxl2Engine {
    //wrapped_engine: Rc<RefCell<Engine>>,
}

impl Smolpxl2Engine {
    fn new(ui: Smolpxl2Ui, game: Smolpxl2Game) -> Self {
        let engine = Rc::new(RefCell::new(Engine::new(ui, game.wrapped_game)));
        let mut engine_b = engine.borrow_mut();
        let ui = engine_b.ui();

        setup_render(&engine, ui);
        setup_frame(&engine, ui);
        setup_event_handler(&engine, ui);

        engine_b.switch_mode();

        Self {
            //wrapped_engine: engine,
        }
    }
}

fn setup_render(engine: &Rc<RefCell<Engine>>, ui: &Smolpxl2Ui) {
    let engine_clone = engine.clone();
    let f = Closure::wrap(Box::new(
        move |canvas_rendering_context2d: CanvasRenderingContext2d,
              width: u32,
              height: u32| {
            engine_clone.borrow_mut().render(
                canvas_rendering_context2d,
                width,
                height,
            );
        },
    )
        as Box<dyn FnMut(CanvasRenderingContext2d, u32, u32)>);

    ui.set_render(f.as_ref().unchecked_ref());

    // Leak this function. We only call this once so it's fine to do this.
    f.forget();
}

fn setup_frame(engine: &Rc<RefCell<Engine>>, ui: &Smolpxl2Ui) {
    let engine_clone = engine.clone();
    let f = Closure::wrap(Box::new(
        move |ts: f64,
              canvas_rendering_context2d: CanvasRenderingContext2d,
              width: u32,
              height: u32| {
            engine_clone.borrow_mut().frame(
                ts,
                canvas_rendering_context2d,
                width,
                height,
            );
        },
    )
        as Box<dyn FnMut(f64, CanvasRenderingContext2d, u32, u32)>);

    ui.set_frame(f.as_ref().unchecked_ref());

    // Leak this function. We only call this once so it's fine to do this.
    f.forget();
}

fn setup_event_handler(engine: &Rc<RefCell<Engine>>, ui: &Smolpxl2Ui) {
    let engine_clone = engine.clone();
    let f = Closure::wrap(Box::new(move |ev: JsValue| {
        engine_clone.borrow_mut().on_event(Evt::from_js_value(ev));
    }) as Box<dyn FnMut(JsValue)>);

    ui.set_event_handler(f.as_ref().unchecked_ref());

    // Leak this function. We only call this once so it's fine to do this.
    f.forget();
}

#[wasm_bindgen]
pub struct Smolpxl2Game {
    wrapped_game: Rc<RefCell<dyn Game>>,
}

impl Smolpxl2Game {
    fn new(game: Rc<RefCell<dyn Game>>) -> Self {
        Self { wrapped_game: game }
    }
}

#[wasm_bindgen]
pub fn new_engine(ui: JsValue, game: Smolpxl2Game) -> Smolpxl2Engine {
    set_panic_hook();
    Smolpxl2Engine::new(ui.unchecked_into(), game)
}

#[wasm_bindgen]
pub fn new_example_game() -> Smolpxl2Game {
    set_panic_hook();
    Smolpxl2Game::new(Rc::new(RefCell::new(ReflowerGame::new())))
}

#[wasm_bindgen]
pub fn new_example2_game() -> Smolpxl2Game {
    set_panic_hook();
    Smolpxl2Game::new(Rc::new(RefCell::new(SnakeGame::new())))
}

fn set_panic_hook() {
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();
}
