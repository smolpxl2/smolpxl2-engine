pub enum TitlePage {
    Text(TextTitlePage),
    HighScoreTable(HighScoreTableTitlePage),
}
impl TitlePage {
    pub fn to_menu(&self) -> &Vec<Vec<String>> {
        match self {
            TitlePage::Text(page) => page.to_menu(),
            TitlePage::HighScoreTable(page) => page.to_menu(),
        }
    }
}

pub struct TextTitlePage {
    text: Vec<Vec<String>>,
}

impl TextTitlePage {
    pub fn new_column(items: impl IntoIterator<Item = String>) -> Self {
        Self {
            text: items.into_iter().map(|item| vec![item]).collect(),
        }
    }

    fn to_menu(&self) -> &Vec<Vec<String>> {
        &self.text
    }
}

pub struct HighScoreTableTitlePage {
    rendered: Vec<Vec<String>>,
}

impl HighScoreTableTitlePage {
    fn to_menu(&self) -> &Vec<Vec<String>> {
        &self.rendered
    }
}
