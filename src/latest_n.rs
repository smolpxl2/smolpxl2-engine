use crate::input::Input;
use crate::input_filter::InputFilter;

use std::collections::{HashMap, VecDeque};

pub struct LatestN {
    // How many inputs are allowed per player
    n: usize,
}

impl LatestN {
    pub fn new(n: usize) -> Self {
        Self { n }
    }
}

impl InputFilter for LatestN {
    fn process(&mut self, inputs: Vec<Input>) -> Vec<Input> {
        // Count of inputs for each player
        let mut player_inputs = HashMap::new();

        let mut ret = VecDeque::new();

        for input in inputs.into_iter().rev() {
            let count = player_inputs
                .entry(input.player())
                .and_modify(|c| *c += 1)
                .or_insert(1);

            if *count <= self.n {
                ret.push_front(input);
            }
        }

        ret.into()
    }
}
#[cfg(test)]
mod tests {
    use crate::{
        input::Input, input_processor::InputProcessor, latest_n::LatestN,
    };

    #[test]
    fn one_players_count_does_not_affect_another() {
        let inputs = vec![
            inp(0, "ArrowUp"),
            inp(0, "ArrowDown"),
            inp(0, "ArrowLeft"),
            inp(1, "ArrowUp"),
            inp(0, "ArrowUp"),
            inp(0, "ArrowRight"),
        ];

        let mut input_processor =
            InputProcessor::new().layer(Box::new(LatestN::new(3)));

        let processed: Vec<_> = input_processor.process(inputs);

        assert_eq!(
            processed,
            &[
                inp(0, "ArrowLeft"),
                inp(1, "ArrowUp"),
                inp(0, "ArrowUp"),
                inp(0, "ArrowRight")
            ]
        );
    }

    #[test]
    fn multiple_players_can_be_limited() {
        let inputs = vec![
            inp(0, "ArrowUp"),
            inp(0, "ArrowDown"),
            inp(0, "ArrowLeft"),
            inp(1, "ArrowUp"),
            inp(1, "ArrowLeft"),
            inp(1, "ArrowUp"),
            inp(1, "ArrowLeft"),
            inp(0, "ArrowUp"),
            inp(0, "ArrowRight"),
        ];

        let mut input_processor =
            InputProcessor::new().layer(Box::new(LatestN::new(3)));

        let processed: Vec<_> = input_processor.process(inputs);

        assert_eq!(
            processed,
            &[
                inp(0, "ArrowLeft"),
                inp(1, "ArrowLeft"),
                inp(1, "ArrowUp"),
                inp(1, "ArrowLeft"),
                inp(0, "ArrowUp"),
                inp(0, "ArrowRight")
            ]
        );
    }

    fn inp(player: u32, button: &str) -> Input {
        Input::PlayerInputDown {
            player,
            button: button.to_owned(),
        }
    }
}
