use super::{
    reflower_images::ReflowerImages,
    reflower_model::{ReflowerModel, ReflowerPlayer},
};
use crate::{
    color::Color,
    game::Game,
    game_state::{
        custom_setting::CustomSetting, game_settings::ControlsMode,
        gs::GameState,
    },
    input::Input,
    screen::Screen,
    screen_creator::ScreenCreator,
};

pub struct ReflowerGame {
    images: ReflowerImages,
}

impl ReflowerGame {
    pub fn new() -> Self {
        Self {
            images: ReflowerImages::new(),
        }
    }
}

impl Game for ReflowerGame {
    fn initial_model(&self) -> serde_json::Value {
        serde_json::to_value(ReflowerModel::default())
            .expect("Failed to serialize initial model!")
    }

    fn view(
        &self,
        screen_creator: ScreenCreator,
        _game_state: &GameState,
        model_json: &serde_json::Value,
    ) -> Option<Screen> {
        let mut screen = screen_creator.create();
        let model: ReflowerModel = serde_json::from_value(model_json.clone())
            .expect("Failed to deserialize model!");

        screen.set_all(&Color::new(255, 255, 128));

        for y in 0..(3 * screen.height() / 4) {
            for x in 0..screen.width() {
                let r = 256 * x / screen.width();
                screen.set(x, y, &Color::new(r as u8, 0, 0));
            }
        }

        for player in model.players {
            screen.draw_image(
                player.x as i32 * 5,
                player.y as i32 * 5,
                &self.images.c1_right,
            );
        }

        Some(screen)
    }

    fn update(
        &mut self,
        game_state: &mut GameState,
        model_json: serde_json::Value,
    ) -> serde_json::Value {
        let mut model: ReflowerModel = serde_json::from_value(model_json)
            .expect("Failed to deserialize model!");

        let mut keep_inputs_for_next_update = Vec::new();
        let mut processed_players = vec![false; model.players.len()];

        for input in game_state.inputs.take_all() {
            match input {
                Input::EngineStarting => init(game_state),
                Input::GameStarting => start_game(game_state, &mut model),
                Input::PlayerInputDown { player: p, button } => {
                    let player = p as usize;
                    // If we receive input for a player we don't have (e.g.
                    // because the user created a new player mid-game) we
                    // ignore it.
                    if player < processed_players.len() {
                        if processed_players[player] {
                            // If we've already processed this player, store
                            // up their input for next time.
                            keep_inputs_for_next_update.push(
                                Input::PlayerInputDown { player: p, button },
                            );
                        } else {
                            process_input_down(&mut model, player, &button);
                            processed_players[player] = true;
                        }
                    }
                }
                _ => (),
            }
        }

        game_state.inputs.extend(keep_inputs_for_next_update);

        serde_json::to_value(model).expect("Failed to serialize model!")
    }
}

fn process_input_down(model: &mut ReflowerModel, player: usize, button: &str) {
    match button {
        "ArrowLeft" => model.players[player].x -= 1,
        "ArrowRight" => model.players[player].x += 1,
        "ArrowUp" => model.players[player].y -= 1,
        "ArrowDown" => model.players[player].y += 1,
        _ => (),
    }
}

fn init(game_state: &mut GameState) {
    let name = game_state.name.edit();
    *name = Some("Reflower".to_owned());

    let game_settings = game_state.game_settings.edit();
    game_settings.size.width = 50;
    game_settings.size.height = 50;
    game_settings.smolpxl_bar = true;
    game_settings.default_controls_mode = ControlsMode::Minimized;

    let player_settings = game_state.player_settings.edit();
    player_settings.min_players = 2;
    player_settings
        .custom_settings
        .push(CustomSetting::new("fave_food", "Fave food"));

    // To add custom menu items, add code like this here:
    //let menu = game_state.menu.edit();
    //menu.append_sublevel(&[], "foo");
    // Note: at the moment, these will always appear at the
    // beginning of the menu.
}

fn start_game(game_state: &mut GameState, model: &mut ReflowerModel) {
    model.players = game_state
        .player_settings
        .view()
        .player_details
        .iter()
        .map(|_details| ReflowerPlayer::new(3, 3))
        .collect()
}

#[cfg(test)]
mod test {
    use crate::{
        example::reflower_model::{ReflowerModel, ReflowerPlayer},
        game::Game,
        game_state::gs::GameState,
        input::Input,
    };

    use super::ReflowerGame;

    #[test]
    fn player_moves_when_button_pressed() {
        // Given a model with player at 5,5
        let mut model = ReflowerModel::default();
        model.players.push(ReflowerPlayer { x: 5, y: 5 });
        let mut game_state = GameState::default();
        let mut game = ReflowerGame::new();
        let model_json = serde_json::to_value(model).unwrap();

        // When I press left
        game_state.inputs.push(Input::PlayerInputDown {
            player: 0,
            button: "ArrowLeft".to_owned(),
        });
        let model_json = game.update(&mut game_state, model_json);

        // The player moves left
        let model =
            serde_json::from_value::<ReflowerModel>(model_json).unwrap();
        assert_eq!(model.players[0].x, 4);
        assert_eq!(model.players[0].y, 5);
    }

    #[test]
    fn multiple_inputs_are_queued() {
        // Given a model with the player at 6, 6
        let mut model = ReflowerModel::default();
        model.players.push(ReflowerPlayer { x: 6, y: 6 });
        let mut game_state = GameState::default();
        let mut game = ReflowerGame::new();
        let model_json = serde_json::to_value(model).unwrap();

        // When I press three keys
        game_state.inputs.push(Input::PlayerInputDown {
            player: 0,
            button: "ArrowRight".to_owned(),
        });
        game_state.inputs.push(Input::PlayerInputDown {
            player: 0,
            button: "ArrowDown".to_owned(),
        });
        game_state.inputs.push(Input::PlayerInputDown {
            player: 0,
            button: "ArrowUp".to_owned(),
        });

        // Then they are processed once each time step
        let model_json = game.update(&mut game_state, model_json);
        let model =
            serde_json::from_value::<ReflowerModel>(model_json).unwrap();
        assert_eq!(model.players[0].x, 7);
        assert_eq!(model.players[0].y, 6);

        let model_json = serde_json::to_value(model).unwrap();
        let model_json = game.update(&mut game_state, model_json);
        let model =
            serde_json::from_value::<ReflowerModel>(model_json).unwrap();
        assert_eq!(model.players[0].x, 7);
        assert_eq!(model.players[0].y, 7);

        let model_json = serde_json::to_value(model).unwrap();
        let model_json = game.update(&mut game_state, model_json);
        let model =
            serde_json::from_value::<ReflowerModel>(model_json).unwrap();
        assert_eq!(model.players[0].x, 7);
        assert_eq!(model.players[0].y, 6);
    }
}
