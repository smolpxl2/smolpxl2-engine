use image::DynamicImage;

use crate::decode_image::decode_image;

pub struct ReflowerImages {
    pub c1_right: DynamicImage,
}

impl ReflowerImages {
    pub fn new() -> Self {
        Self {
            c1_right: decode_image(include_bytes!("pngs/c1_right.png")),
        }
    }
}
