use serde::{Deserialize, Serialize};

#[derive(Default, Deserialize, Serialize)]
pub struct ReflowerModel {
    pub players: Vec<ReflowerPlayer>,
}

#[derive(Deserialize, Serialize)]
pub struct ReflowerPlayer {
    pub x: u32,
    pub y: u32,
}

impl ReflowerPlayer {
    pub fn new(x: u32, y: u32) -> Self {
        Self { x, y }
    }
}
