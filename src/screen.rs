use image::{DynamicImage, RgbaImage};
use imageproc::rect::Rect;

use crate::color::Color;

pub struct Screen {
    pixels: RgbaImage,
}

impl Screen {
    pub fn new(width: u32, height: u32) -> Screen {
        Self {
            pixels: RgbaImage::new(width, height),
        }
    }

    pub fn pixels(&self) -> &[u8] {
        &self.pixels
    }

    pub fn width(&self) -> u32 {
        self.pixels.width()
    }

    pub fn height(&self) -> u32 {
        self.pixels.height()
    }

    pub fn set(&mut self, x: u32, y: u32, color: &Color) {
        if x < self.width() && y < self.height() {
            self.pixels.put_pixel(x, y, color.into());
        }
    }

    pub(crate) fn rect(
        &mut self,
        left: u32,
        top: u32,
        width: u32,
        height: u32,
        color: &Color,
    ) {
        self.pixels = imageproc::drawing::draw_filled_rect(
            &self.pixels,
            Rect::at(left as i32, top as i32).of_size(width, height),
            color.into(),
        );
    }

    pub fn set_all(&mut self, color: &Color) {
        self.pixels =
            RgbaImage::from_pixel(self.width(), self.height(), color.into());
    }

    pub fn draw_image(&mut self, x: i32, y: i32, image: &DynamicImage) {
        image::imageops::overlay(&mut self.pixels, image, x.into(), y.into());
    }
}
