use crate::screen::Screen;

pub struct ScreenCreator {
    width: u32,
    height: u32,
}

impl ScreenCreator {
    pub fn new(width: u32, height: u32) -> Self {
        Self { width, height }
    }

    pub fn create(&self) -> Screen {
        Screen::new(self.width, self.height)
    }
}
