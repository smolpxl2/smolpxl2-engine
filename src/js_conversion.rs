use wasm_bindgen::JsValue;

use crate::color::Color;

/**
 * https://stackoverflow.com/a/61674699
 *
 * TODO: move this into a wrapper struct around Smolpxl2Ui
 */
pub fn array_of_str(values: &[&str]) -> JsValue {
    JsValue::from(
        values
            .iter()
            .map(|x| JsValue::from_str(x))
            .collect::<js_sys::Array>(),
    )
}

/**
 * TODO: move this into a wrapper struct around Smolpxl2Ui
 */
pub fn array_of_string(values: &[String]) -> JsValue {
    JsValue::from(
        values
            .iter()
            .map(|x| JsValue::from_str(x))
            .collect::<js_sys::Array>(),
    )
}

/**
 * https://stackoverflow.com/a/61674699
 *
 * TODO: move this into a wrapper struct around Smolpxl2Ui
 */
pub fn array_of_array_of_string(values: &[Vec<String>]) -> JsValue {
    JsValue::from(
        values
            .iter()
            .map(|x| {
                JsValue::from(
                    x.iter()
                        .map(|y| JsValue::from_str(y))
                        .collect::<js_sys::Array>(),
                )
            })
            .collect::<js_sys::Array>(),
    )
}

pub fn array_of_color(values: &[Color]) -> JsValue {
    let ret = js_sys::Array::new();
    for v in values {
        let c = js_sys::Array::new();
        c.push(&JsValue::from(v.r));
        c.push(&JsValue::from(v.g));
        c.push(&JsValue::from(v.b));
        ret.push(&JsValue::from(c));
    }
    JsValue::from(&ret)
}
