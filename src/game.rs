use crate::game_state::gs::GameState;
use crate::screen::Screen;
use crate::screen_creator::ScreenCreator;

pub trait Game {
    /**
     * Create a game model. This will be stored by the engine, and supplied as
     * part of the GameState whenever view, update or new_game are called.
     *
     * This model stores the custom information that is specific to your game.
     * For example, the positions and health of the players.
     */
    fn initial_model(&self) -> serde_json::Value;

    /**
     * Draw the game to the screen. For example:
     *
     * ```
     * # use smolpxl2_engine::screen_creator::ScreenCreator;
     * use smolpxl2_engine::color::Color;
     * # let screen_creator = ScreenCreator::new(50, 50);
     * let mut screen = screen_creator.create();
     * screen.set(10, 10, &Color::new(250, 128, 128));
     * ```
     */
    fn view(
        &self,
        screen_creator: ScreenCreator,
        game_state: &GameState,
        model_json: &serde_json::Value,
    ) -> Option<Screen>;

    /**
     * Update the model for the next frame.
     */
    fn update(
        &mut self,
        game_state: &mut GameState,
        model_json: serde_json::Value,
    ) -> serde_json::Value;
}
