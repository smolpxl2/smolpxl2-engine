#[derive(Clone, Debug, PartialEq)]
pub enum Input {
    EngineStarting,
    GameStarting,
    PlayerCreated { player: u32 },
    PlayerInputDown { player: u32, button: String },
    PlayerInputUp { player: u32, button: String },
    GeneralInputDown { button: String },
    GeneralInputUp { button: String },
    UnmappedKeyDown { key: String },
    UnmappedKeyUp { key: String },
}

impl Input {
    /// Return which player this input is for, or None if it is a general input
    pub(crate) fn player(&self) -> Option<u32> {
        match self {
            Input::PlayerInputDown { player, .. } => Some(*player),
            Input::PlayerInputUp { player, .. } => Some(*player),
            _ => None,
        }
    }
}
