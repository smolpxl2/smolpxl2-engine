use std::{
    collections::{vec_deque, VecDeque},
    ops::{Add, SubAssign},
};

use rand::{seq::SliceRandom, Rng, RngCore};
use serde::{Deserialize, Serialize};

use crate::{
    color::Color,
    game::Game,
    game_state::{
        game_settings::ControlsMode, gs::GameState, player_input::PlayerInputs,
    },
    input::Input,
    input_filter::InputFilter,
    input_processor::InputProcessor,
    latest_n::LatestN,
    repeat_remover::RepeatRemover,
    screen::Screen,
    screen_creator::ScreenCreator,
};

const APPLE_COLOR: Color = Color::new(255, 0, 0);
const WALL_COLOR: Color = Color::new(128, 128, 128);

pub struct SnakeGame {
    input_processor: InputProcessor,
}

impl SnakeGame {
    pub fn new() -> Self {
        Self {
            input_processor: InputProcessor::new()
                .layer(Box::new(SnakeInputTypes::new()))
                .layer(Box::new(RepeatRemover::new()))
                .layer(Box::new(LatestN::new(3))),
        }
    }

    fn process_inputs(
        &mut self,
        game_state: &mut GameState,
        model: &mut SnakeModel,
    ) {
        let mut keep_inputs_for_next_update = Vec::new();
        let mut processed_players = vec![0; model.players.len()];

        let inputs = game_state.inputs.take_all();
        let inputs = self.input_processor.process(inputs);

        for input in inputs {
            match input {
                Input::EngineStarting => init(game_state),
                Input::GameStarting => start_game(game_state, model),
                Input::PlayerCreated { player } => {
                    new_player(game_state, player)
                }
                Input::PlayerInputDown { player: p, button } => {
                    let player = p as usize;
                    // If we receive input for a player we don't have (e.g.
                    // because the user created a new player mid-game) we
                    // ignore it.
                    if player < processed_players.len() {
                        if processed_players[player] > 0 {
                            // If we've already processed this player, store
                            // up their input for next time.
                            keep_inputs_for_next_update.push(
                                Input::PlayerInputDown { player: p, button },
                            );
                            processed_players[player] += 1;
                        } else {
                            process_input_down(model, player, &button);
                            processed_players[player] += 1;
                        }
                    }
                }
                _ => (),
            }
        }

        game_state.inputs.extend(keep_inputs_for_next_update);
    }
}

fn new_player<R: RngCore>(game_state: &mut GameState<R>, player_index: u32) {
    let p = player_index as usize;
    game_state.player_settings.edit().player_details[p].color =
        Some(color_by_index(p as usize));
}

impl Default for SnakeGame {
    fn default() -> Self {
        Self::new()
    }
}

impl Game for SnakeGame {
    fn initial_model(&self) -> serde_json::Value {
        serde_json::to_value(SnakeModel::default())
            .expect("Failed to serialize initial model!")
    }

    fn view(
        &self,
        screen_creator: ScreenCreator,
        _game_state: &GameState,
        model_json: &serde_json::Value,
    ) -> Option<Screen> {
        let mut screen = screen_creator.create();
        let model: SnakeModel = serde_json::from_value(model_json.clone())
            .expect("Failed to deserialize model!");

        screen.rect(0, 0, WIDTH as u32, 1, &WALL_COLOR);
        screen.rect(0, 0, 1, HEIGHT as u32, &WALL_COLOR);
        screen.rect((WIDTH - 1) as u32, 0, 1, HEIGHT as u32, &WALL_COLOR);
        screen.rect(0, (HEIGHT - 1) as u32, WIDTH as u32, 1, &WALL_COLOR);

        for player in &model.players {
            player.view_body(&mut screen)
        }

        screen.set(model.apple.x as u32, model.apple.y as u32, &APPLE_COLOR);

        for player in &model.players {
            player.view_head(&mut screen)
        }

        Some(screen)
    }

    fn update(
        &mut self,
        game_state: &mut GameState,
        model_json: serde_json::Value,
    ) -> serde_json::Value {
        let mut model: SnakeModel = serde_json::from_value(model_json)
            .expect("Failed to deserialize model!");

        self.process_inputs(game_state, &mut model);
        update_snakes(&mut model);
        check_for_collision(&mut model, &mut game_state.rng);

        serde_json::to_value(model).expect("Failed to serialize model!")
    }
}

fn update_snakes(model: &mut SnakeModel) {
    for player in &mut model.players {
        player.update();
    }
}

fn check_for_collision(model: &mut SnakeModel, rng: &mut impl Rng) {
    let mut move_apple = false;
    let mut kill_list = Vec::new();
    let mut grow_list = Vec::new();
    for (i, player) in model.living_players_enumerated() {
        if touching_wall(player.body.head()) {
            kill_list.push(i);
        } else if *player.body.head() == model.apple {
            move_apple = true;
            grow_list.push(i);
        } else {
            for other in model.living_players() {
                if other.body.tail_touches(&player.body.head()) {
                    kill_list.push(i);
                }
            }
        }
    }

    for i in kill_list {
        assert!(model.players[i].is_alive());
        model.players[i].die();
    }

    for i in grow_list {
        assert!(model.players[i].is_alive());
        model.players[i].extend();
    }

    if move_apple {
        model.apple = Point::new_random(rng);
    }
}

fn touching_wall(point: &Point) -> bool {
    point.x <= 0
        || point.y <= 0
        || point.x >= WIDTH - 1
        || point.y >= HEIGHT - 1
}

fn process_input_down(model: &mut SnakeModel, player: usize, button: &str) {
    let new_dir = match button {
        "ArrowLeft" => Some(Direction::Left),
        "ArrowRight" => Some(Direction::Right),
        "ArrowUp" => Some(Direction::Up),
        "ArrowDown" => Some(Direction::Down),
        _ => None,
    };
    if let Some(new_dir) = new_dir {
        let dir = &mut model.players[player].direction;
        if *dir != new_dir.opposite() {
            *dir = new_dir;
        }
    }
}

const WIDTH: u8 = 20;
const HEIGHT: u8 = 20;

fn init(game_state: &mut GameState) {
    let name = game_state.name.edit();
    *name = Some("Snake".to_owned());

    let game_settings = game_state.game_settings.edit();
    game_settings.size.width = WIDTH as u32;
    game_settings.size.height = HEIGHT as u32;
    game_settings.smolpxl_bar = true;
    game_settings.default_controls_mode = ControlsMode::Overlay;
    game_settings.set_fps(3);

    let player_settings = game_state.player_settings.edit();
    player_settings.min_players = 1;
    player_settings.max_players = 4;
    player_settings.inputs = PlayerInputs::arrows();
}

fn start_game<R>(game_state: &mut GameState<R>, model: &mut SnakeModel)
where
    R: Rng,
{
    let player_settings = game_state.player_settings.view();

    let num_players = player_settings.player_details.len();

    let mut indices: Vec<usize> = (0..num_players).collect();
    indices.shuffle(&mut game_state.rng);

    model.players = player_settings
        .player_details
        .iter()
        .zip(indices.into_iter())
        .map(|(details, i)| {
            SnakePlayer::new_by_index(
                i,
                num_players,
                details
                    .color
                    .as_ref()
                    .expect("All players should have a color!")
                    .clone(),
            )
        })
        .collect();

    model.apple = Point::new_random(&mut game_state.rng);
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
struct SnakeModel {
    players: Vec<SnakePlayer>,
    apple: Point,
}

impl SnakeModel {
    fn living_players(&self) -> impl Iterator<Item = &SnakePlayer> {
        self.players.iter().filter(|p| p.is_alive())
    }

    fn living_players_enumerated(
        &self,
    ) -> impl Iterator<Item = (usize, &SnakePlayer)> {
        self.players
            .iter()
            .enumerate()
            .filter(|(_, p)| p.is_alive())
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
enum Life {
    Alive,
    Dead(u32),
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
struct SnakePlayer {
    body: SnakeBody,
    direction: Direction,
    color: Color,
    head_color: Color,
    life: Life,
}

impl SnakePlayer {
    fn new_by_index(start_pos_index: usize, num: usize, color: Color) -> Self {
        let (position, direction) = match num {
            1 => match start_pos_index {
                0 => (Point { x: 10, y: 12 }, Direction::Up),
                _ => todo!("Invalid index!"),
            },
            2 => match start_pos_index {
                0 => (Point { x: 5, y: 8 }, Direction::Down),
                1 => (Point { x: 14, y: 12 }, Direction::Up),
                _ => todo!("Invalid index!"),
            },
            3 => match start_pos_index {
                0 => (Point { x: 3, y: 6 }, Direction::Down),
                1 => (Point { x: 10, y: 13 }, Direction::Up),
                2 => (Point { x: 16, y: 6 }, Direction::Down),
                _ => todo!("Invalid index!"),
            },
            4 => match start_pos_index {
                0 => (Point { x: 6, y: 3 }, Direction::Right),
                1 => (Point { x: 16, y: 6 }, Direction::Down),
                2 => (Point { x: 13, y: 16 }, Direction::Left),
                3 => (Point { x: 3, y: 13 }, Direction::Up),
                _ => todo!("Invalid index!"),
            },
            _ => panic!("Can't have more than 4 players!"),
        };

        let head_color = head_color(&color);

        Self {
            body: SnakeBody::new(position, direction),
            direction,
            color,
            head_color,
            life: Life::Alive,
        }
    }

    fn update(&mut self) {
        self.life = match self.life {
            Life::Alive => {
                self.body.update(self.direction);
                Life::Alive
            }
            Life::Dead(t) => Life::Dead(t + 1),
        }
    }

    fn is_alive(&self) -> bool {
        matches!(self.life, Life::Alive)
    }

    fn die(&mut self) {
        self.life = Life::Dead(0);
    }

    fn body_color(&self) -> Option<Color> {
        match self.life {
            Life::Alive => Some(self.color.clone()),
            Life::Dead(t) => fade_color(&self.color, t),
        }
    }

    fn head_color(&self) -> Option<Color> {
        match self.life {
            Life::Alive => Some(self.head_color.clone()),
            Life::Dead(_) => None,
        }
    }

    fn view_body(&self, screen: &mut Screen) {
        let c = self.body_color();
        if let Some(c) = c {
            for p in self.body.into_iter().skip(1) {
                screen.set(p.x as u32, p.y as u32, &c);
            }
        }
    }

    fn view_head(&self, screen: &mut Screen) {
        let head = self.head();
        let c = self.head_color();
        if let Some(c) = c {
            screen.set(head.x as u32, head.y as u32, &c);
        }
    }

    fn head(&self) -> &Point {
        self.body.head()
    }

    fn extend(&mut self) {
        self.body.extend();
    }
}

fn fade_color(color: &Color, t: u32) -> Option<Color> {
    const MAX_T: u32 = 10;
    if t >= MAX_T {
        return None;
    }
    let s = 0.5 - 0.5 * (t as f32 / MAX_T as f32);

    let scale = |v| (v as f32 * s) as u8;

    Some(Color::new(scale(color.r), scale(color.g), scale(color.b)))
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
struct SnakeBody(VecDeque<Point>);

impl SnakeBody {
    fn new(mut position: Point, direction: Direction) -> SnakeBody {
        let mut points = VecDeque::new();

        for _ in 0..5 {
            points.push_back(position.clone());
            position -= direction;
        }

        Self(points)
    }

    fn head(&self) -> &Point {
        &self.0.front().unwrap()
    }

    fn update(&mut self, direction: Direction) {
        let new_head = self.head().clone() + direction;
        self.0.push_front(new_head);
        self.0.pop_back();
    }

    #[cfg(test)]
    fn len(&self) -> usize {
        self.0.len()
    }

    fn extend(&mut self) {
        let b = self.0.back().unwrap().clone();
        for _ in 0..4 {
            self.0.push_back(b.clone())
        }
        self.0.push_back(b)
    }

    fn tail_touches(&self, point: &Point) -> bool {
        self.0.iter().skip(1).any(|p| p == point)
    }
}

impl<'a> IntoIterator for &'a SnakeBody {
    type Item = &'a Point;

    type IntoIter = vec_deque::Iter<'a, Point>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

fn color_by_index(start_pos_index: usize) -> Color {
    match start_pos_index {
        // Green
        0 => Color::new(0, 190, 0),
        // Blue
        1 => Color::new(50, 50, 190),
        // Yellow
        2 => Color::new(190, 190, 0),
        // Purple
        3 => Color::new(190, 0, 190),
        // Grey :-(
        _ => Color::new(190, 190, 190),
    }
}

fn head_color(color: &Color) -> Color {
    match color {
        Color { r: 0, g: 190, b: 0 } => Color::new(0, 255, 0),
        Color {
            r: 50,
            g: 50,
            b: 190,
        } => Color::new(100, 100, 255),
        Color {
            r: 190,
            g: 190,
            b: 0,
        } => Color::new(255, 255, 0),
        Color {
            r: 190,
            g: 0,
            b: 190,
        } => Color::new(255, 0, 255),
        _ => {
            panic!("Unknown colour {:?}", color);
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Serialize, Deserialize, PartialEq)]
enum Direction {
    #[default]
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn opposite(&self) -> Self {
        match self {
            Direction::Left => Self::Right,
            Direction::Right => Self::Left,
            Direction::Up => Self::Down,
            Direction::Down => Self::Up,
        }
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
struct Point {
    x: u8,
    y: u8,
}

impl Point {
    fn new_random(rng: &mut impl Rng) -> Self {
        Self {
            x: rng.gen_range(1..WIDTH - 2),
            y: rng.gen_range(1..WIDTH - 2),
        }
    }

    #[cfg(test)]
    fn new(x: u8, y: u8) -> Self {
        Self { x, y }
    }
}

impl SubAssign<Direction> for Point {
    fn sub_assign(&mut self, direction: Direction) {
        match direction {
            Direction::Up => self.y += 1,
            Direction::Down => self.y -= 1,
            Direction::Left => self.x += 1,
            Direction::Right => self.x -= 1,
        };
    }
}

impl Add<Direction> for Point {
    type Output = Point;

    fn add(self, direction: Direction) -> Self::Output {
        match direction {
            Direction::Up => Point {
                x: self.x,
                y: self.y - 1,
            },
            Direction::Down => Point {
                x: self.x,
                y: self.y + 1,
            },
            Direction::Left => Point {
                x: self.x - 1,
                y: self.y,
            },
            Direction::Right => Point {
                x: self.x + 1,
                y: self.y,
            },
        }
    }
}

struct SnakeInputTypes {}

impl SnakeInputTypes {
    fn new() -> Self {
        Self {}
    }
}

impl InputFilter for SnakeInputTypes {
    fn process(&mut self, inputs: Vec<Input>) -> Vec<Input> {
        inputs
            .into_iter()
            .filter(|input| match input {
                Input::EngineStarting => true,
                Input::GameStarting => true,
                Input::PlayerCreated { .. } => true,
                Input::PlayerInputDown { .. } => true,
                _ => false,
            })
            .collect()
    }
}

#[cfg(test)]
mod test {
    use rand::RngCore;

    use crate::input::Input;
    use crate::input_processor::tests::buts;

    use super::*;

    #[test]
    fn can_subtract_directions_from_points() {
        let mut p = pt(3, 5);

        p -= Direction::Up;
        assert_eq!(p, pt(3, 6));

        p -= Direction::Left;
        assert_eq!(p, pt(4, 6));

        p -= Direction::Right;
        assert_eq!(p, pt(3, 6));

        p -= Direction::Down;
        assert_eq!(p, pt(3, 5));
    }

    #[test]
    fn directions_have_opposites() {
        assert_eq!(Direction::Left.opposite(), Direction::Right);
        assert_eq!(Direction::Right.opposite(), Direction::Left);
        assert_eq!(Direction::Up.opposite(), Direction::Down);
        assert_eq!(Direction::Down.opposite(), Direction::Up);
    }

    #[test]
    fn initial_apple_position_is_random() {
        // Fake some random numbers that give us the apple positions we expect
        let rng = FakeRng::new(&[873343455, 3000033434]);

        // Given a game state with an Rng
        let mut game_state = GameState::new(rng);

        // When we start a snake game
        let mut model = SnakeModel::default();
        start_game(&mut game_state, &mut model);

        // Then the apple is placed "randomly"
        // (The above fake random numbers result in these co-ordinates
        // in my implementation.)
        assert_eq!(model.apple.x, 4);
        assert_eq!(model.apple.y, 12);
    }

    #[test]
    fn player_start_indices_are_random() {
        // Fake some random numbers that give us the order we expect
        let rng = FakeRng::new(&[101, 0, 0]);

        // Given we have two players
        let mut game_state = GameState::new(rng);
        add_player(&mut game_state);
        add_player(&mut game_state);

        // When we start a snake game
        let mut model = SnakeModel::default();
        start_game(&mut game_state, &mut model);

        // Then the players are assigned start position indices "randomly"
        assert_eq!(model.players[0].head().x, 14);
        assert_eq!(model.players[0].head().y, 12);
        assert_eq!(model.players[1].head().x, 5);
        assert_eq!(model.players[1].head().y, 8);
    }

    #[test]
    fn multiple_players_start_at_fixed_positions() {
        // Given we have three players
        let mut game_state = GameState::default();
        add_player(&mut game_state);
        add_player(&mut game_state);
        add_player(&mut game_state);

        // When we start a snake game
        let mut model = SnakeModel::default();
        start_game(&mut game_state, &mut model);

        // Then the players are distributed nicely
        assert!(contains_player_at_pos(&model, pt(3, 6), Direction::Down));
        assert!(contains_player_at_pos(&model, pt(10, 13), Direction::Up));
        assert!(contains_player_at_pos(&model, pt(16, 6), Direction::Down));

        // And when we add another
        add_player(&mut game_state);
        start_game(&mut game_state, &mut model);

        // Then the players are redistributed
        assert!(contains_player_at_pos(&model, pt(6, 3), Direction::Right));
        assert!(contains_player_at_pos(&model, pt(16, 6), Direction::Down));
        assert!(contains_player_at_pos(&model, pt(13, 16), Direction::Left));
        assert!(contains_player_at_pos(&model, pt(3, 13), Direction::Up));
    }

    #[test]
    fn repeated_inputs_are_squashed() {
        let mut game = SnakeGame::new();
        let mut game_state = GameState::default();
        let mut model = SnakeModel::default();
        add_player(&mut game_state);
        start_game(&mut game_state, &mut model);

        game_state.inputs.push(inp(0, "ArrowUp")); // Used
        game_state.inputs.push(inp(0, "ArrowUp")); // Repeat
        game_state.inputs.push(inp(0, "ArrowLeft"));
        game_state.inputs.push(inp(0, "ArrowUp"));
        game_state.inputs.push(inp(0, "ArrowUp")); // Repeat

        game.process_inputs(&mut game_state, &mut model);

        assert_eq!(
            buts(game_state.inputs.take_all()),
            buts([inp(0, "ArrowLeft"), inp(0, "ArrowUp")])
        );
    }

    #[test]
    fn repeated_inputs_for_different_players_are_independent() {
        // Given a game with 2 players
        let mut game = SnakeGame::new();
        let mut game_state = GameState::default();
        let mut model = SnakeModel::default();
        add_player(&mut game_state);
        add_player(&mut game_state);
        start_game(&mut game_state, &mut model);

        // When inputs are pressed for both players
        game_state.inputs.push(inp(0, "ArrowRight")); // Gets used
        game_state.inputs.push(inp(0, "ArrowRight")); // Repeat
        game_state.inputs.push(inp(1, "ArrowRight")); // Gets used
        game_state.inputs.push(inp(1, "ArrowUp"));
        game_state.inputs.push(inp(1, "ArrowLeft"));
        game_state.inputs.push(inp(0, "ArrowDown"));
        game_state.inputs.push(inp(1, "ArrowLeft")); // Repeat
        game_state.inputs.push(inp(0, "ArrowDown")); // Repeat
        game_state.inputs.push(inp(0, "ArrowDown")); // Repeat

        game.process_inputs(&mut game_state, &mut model);

        // Then repeats are squashed for each player individually
        assert_eq!(
            &game_state.inputs.take_all(),
            &[inp(1, "ArrowUp"), inp(1, "ArrowLeft"), inp(0, "ArrowDown")]
        );
    }

    #[test]
    fn not_eating_an_apple_does_nothing() {
        // Given the snake is not eating the apple
        let mut model = SnakeModel::default();
        model.apple = Point::new(5, 7);
        model.players.push(SnakePlayer::new_by_index(
            0,
            1,
            Color::new(0, 190, 0),
        ));
        let player = &model.players[0];
        assert_ne!(model.apple, *player.body.head());
        assert_eq!(player.body.len(), 5);
        let mut rng = FakeRng::new(&[]);

        // When I check for collisions
        check_for_collision(&mut model, &mut rng);

        // Then the apple hasn't moved
        assert_eq!(model.apple, Point::new(5, 7));
        // And the snake hasn't grown
        assert_eq!(model.players[0].body.len(), 5);
    }

    #[test]
    fn dead_snakes_do_not_eat_apples() {
        // Given the snake positioned to eat the apple,
        // but is dead
        let mut model = SnakeModel::default();
        model.players.push(SnakePlayer::new_by_index(
            0,
            1,
            Color::new(0, 190, 0),
        ));
        let player = &mut model.players[0];
        model.apple = player.body.head().clone();
        player.die();
        assert_eq!(player.body.len(), 5);

        // When I check for collisions
        let mut rng = FakeRng::new(&[1, 1]);
        check_for_collision(&mut model, &mut rng);

        // Then the apple does not move
        let player = &model.players[0];
        assert_eq!(model.apple, *player.body.head());
        // And the snake has not grown
        assert_eq!(player.body.len(), 5);
    }

    #[test]
    fn eating_an_apple_moves_the_apple_and_lengthens_the_snake() {
        // Given the snake is eating the apple
        let mut model = SnakeModel::default();
        model.players.push(SnakePlayer::new_by_index(
            0,
            1,
            Color::new(0, 190, 0),
        ));
        let player = &model.players[0];
        model.apple = player.body.head().clone();
        assert_eq!(player.body.len(), 5);
        let mut rng = FakeRng::new(&[1, 1]);

        // When I check for collisions
        check_for_collision(&mut model, &mut rng);

        // Then the apple has moved
        let player = &model.players[0];
        assert_ne!(model.apple, *player.body.head());
        // And the snake has grown
        assert_eq!(player.body.len(), 10);
    }

    #[test]
    fn eating_own_tail_kills_you() {
        // Given the snake is eating its own tail
        let mut model = SnakeModel::default();
        model.players.push(SnakePlayer::new_by_index(
            0,
            1,
            Color::new(0, 190, 0),
        ));
        let player = &mut model.players[0];
        player.body = SnakeBody(
            vec![
                Point::new(10, 10),
                Point::new(10, 9),
                Point::new(10, 8),
                Point::new(9, 8),
                Point::new(9, 9),
                Point::new(10, 9),
                Point::new(10, 10),
            ]
            .into(),
        );

        // When I check for collisions
        let mut rng = FakeRng::new(&[]);
        check_for_collision(&mut model, &mut rng);
        let player = &model.players[0];

        // Then the player is dead
        assert!(!player.is_alive());
    }

    #[test]
    fn eating_apple_on_own_tail_does_not_kill_you() {
        // Given the snake is eating its own tail
        // but the apple is in the exact crossing point
        let mut model = SnakeModel::default();
        model.players.push(SnakePlayer::new_by_index(
            0,
            1,
            Color::new(0, 190, 0),
        ));
        let player = &mut model.players[0];
        player.body = SnakeBody(
            vec![
                Point::new(10, 10),
                Point::new(10, 9),
                Point::new(10, 8),
                Point::new(9, 8),
                Point::new(9, 9),
                Point::new(10, 9),
                Point::new(10, 10),
            ]
            .into(),
        );
        model.apple = Point::new(10, 10);

        // When I check for collisions
        let mut rng = FakeRng::new(&[1, 1]);
        check_for_collision(&mut model, &mut rng);
        let player = &model.players[0];

        // Then the player is alive
        assert!(player.is_alive());

        // And the player ate the apple
        let player = &model.players[0];
        assert_ne!(model.apple, *player.body.head());
        assert_eq!(player.body.len(), 12);
    }

    #[test]
    fn two_snakes_can_both_eat_the_same_apple() {
        // Given two snakes are approaching the same apple
        let mut model = SnakeModel::default();
        model.players.push(SnakePlayer::new_by_index(
            0,
            2,
            Color::new(0, 190, 0),
        ));
        model.players.push(SnakePlayer::new_by_index(
            1,
            2,
            Color::new(0, 190, 0),
        ));

        model.players[0].body = SnakeBody(
            vec![Point::new(9, 10), Point::new(8, 10), Point::new(7, 10)]
                .into(),
        );
        model.players[0].direction = Direction::Right;

        model.players[1].body = SnakeBody(
            vec![Point::new(10, 9), Point::new(10, 8), Point::new(10, 7)]
                .into(),
        );
        model.players[1].direction = Direction::Down;

        model.apple = Point::new(10, 10);

        // When we step forward one time step
        let mut rng = FakeRng::new(&[1, 1]);
        update_snakes(&mut model);
        check_for_collision(&mut model, &mut rng);

        // Then both players moved forwards to the same place
        assert_eq!(Point::new(10, 10), *model.players[0].body.head());
        assert_eq!(Point::new(10, 10), *model.players[1].body.head());

        // And both players are alive
        assert!(model.players[0].is_alive());
        assert!(model.players[1].is_alive());

        // And both players ate the apple
        assert_ne!(model.apple, *model.players[0].body.head());
        assert_eq!(model.players[0].body.len(), 8);
        assert_eq!(model.players[1].body.len(), 8);
    }

    #[test]
    fn eating_someone_elses_tail_kills_you() {
        // Given one snake is eating another
        let mut model = SnakeModel::default();
        model.players.push(SnakePlayer::new_by_index(
            0,
            2,
            Color::new(0, 190, 0),
        ));
        model.players.push(SnakePlayer::new_by_index(
            1,
            2,
            Color::new(0, 190, 0),
        ));
        let player1 = &mut model.players[1];
        player1.body = SnakeBody(
            vec![
                Point::new(4, 8),
                Point::new(5, 8),
                Point::new(6, 8),
                Point::new(7, 8),
                Point::new(8, 8),
            ]
            .into(),
        );

        // When I check for collisions
        let mut rng = FakeRng::new(&[]);
        check_for_collision(&mut model, &mut rng);

        // Then player 0 is dead but 1 is not
        assert!(!model.players[0].is_alive());
        assert!(model.players[1].is_alive());
    }

    #[test]
    fn hitting_the_wall_kills_you() {
        // Given 4 snakes are all colliding with the wall
        let mut model = SnakeModel::default();
        model.players.push(SnakePlayer::new_by_index(
            0,
            4,
            Color::new(0, 190, 0),
        ));
        model.players.push(SnakePlayer::new_by_index(
            1,
            4,
            Color::new(0, 190, 0),
        ));
        model.players.push(SnakePlayer::new_by_index(
            2,
            4,
            Color::new(0, 190, 0),
        ));
        model.players.push(SnakePlayer::new_by_index(
            3,
            4,
            Color::new(0, 190, 0),
        ));
        let player0 = &mut model.players[0];
        player0.body = SnakeBody(
            vec![
                Point::new(WIDTH - 1, 8),
                Point::new(WIDTH - 2, 8),
                Point::new(WIDTH - 3, 8),
                Point::new(WIDTH - 4, 8),
            ]
            .into(),
        );
        let player1 = &mut model.players[1];
        player1.body = SnakeBody(
            vec![
                Point::new(0, 8),
                Point::new(1, 8),
                Point::new(2, 8),
                Point::new(3, 8),
            ]
            .into(),
        );
        player1.direction = Direction::Left;
        let player2 = &mut model.players[2];
        player2.body = SnakeBody(
            vec![
                Point::new(2, 0),
                Point::new(2, 1),
                Point::new(2, 2),
                Point::new(2, 3),
            ]
            .into(),
        );
        player2.direction = Direction::Up;
        let player3 = &mut model.players[3];
        player3.body = SnakeBody(
            vec![
                Point::new(2, HEIGHT - 1),
                Point::new(2, HEIGHT - 2),
                Point::new(2, HEIGHT - 3),
                Point::new(2, HEIGHT - 4),
            ]
            .into(),
        );
        player3.direction = Direction::Up;

        // When I check for collisions
        let mut rng = FakeRng::new(&[]);
        check_for_collision(&mut model, &mut rng);

        // Then the players are dead
        assert!(!model.players[0].is_alive());
    }

    #[test]
    fn eating_a_dead_tail_does_nothing() {
        // Given one snake is eating another but it's dead
        let mut model = SnakeModel::default();
        model.players.push(SnakePlayer::new_by_index(
            0,
            2,
            Color::new(0, 190, 0),
        ));
        model.players.push(SnakePlayer::new_by_index(
            1,
            2,
            Color::new(0, 190, 0),
        ));
        let player1 = &mut model.players[1];
        player1.body = SnakeBody(
            vec![
                Point::new(4, 8),
                Point::new(5, 8),
                Point::new(6, 8),
                Point::new(7, 8),
                Point::new(8, 8),
            ]
            .into(),
        );
        player1.die();

        // When I check for collisions
        let mut rng = FakeRng::new(&[]);
        check_for_collision(&mut model, &mut rng);

        // Then player 0 is not dead (and 1 still is)
        assert!(model.players[0].is_alive());
        assert!(!model.players[1].is_alive());
    }

    #[test]
    fn live_snakes_move_each_time_step() {
        // Given 2 snakes facing different ways
        let mut model = SnakeModel::default();
        model.players.push(SnakePlayer::new_by_index(
            0,
            2,
            Color::new(0, 190, 0),
        ));
        model.players.push(SnakePlayer::new_by_index(
            1,
            2,
            Color::new(0, 190, 0),
        ));
        assert_eq!(*model.players[0].body.head(), Point::new(5, 8));
        assert_eq!(*model.players[1].body.head(), Point::new(14, 12));
        assert_eq!(model.players[0].direction, Direction::Down);
        assert_eq!(model.players[1].direction, Direction::Up);

        // When we step one time step
        update_snakes(&mut model);

        // Then they moved forward
        assert_eq!(*model.players[0].body.head(), Point::new(5, 9));
        assert_eq!(*model.players[1].body.head(), Point::new(14, 11));
    }

    #[test]
    fn dead_snake_does_not_move() {
        // Given 2 snakes, one dead
        let mut model = SnakeModel::default();
        model.players.push(SnakePlayer::new_by_index(
            0,
            2,
            Color::new(0, 190, 0),
        ));
        model.players.push(SnakePlayer::new_by_index(
            1,
            2,
            Color::new(0, 190, 0),
        ));
        model.players[0].die();
        assert_eq!(*model.players[0].body.head(), Point::new(5, 8));
        assert_eq!(*model.players[1].body.head(), Point::new(14, 12));

        // When we step one time step
        update_snakes(&mut model);

        // Then only the live snake moves
        assert_eq!(*model.players[0].body.head(), Point::new(5, 8));
        assert_eq!(*model.players[1].body.head(), Point::new(14, 11));
    }

    #[test]
    fn dead_snake_fades_away() {
        // Given a newly-dead snake
        let mut model = SnakeModel::default();
        model.players.push(SnakePlayer::new_by_index(
            0,
            2,
            Color::new(0, 190, 0),
        ));
        model.players[0].die();

        fn pcol(model: &SnakeModel) -> Color {
            model.players[0].body_color().unwrap()
        }

        // When we ask for its color
        // Then it is faded
        assert_eq!(pcol(&model), Color::new(0, 95, 0));

        // And when we step time
        // Then it fades out more
        update_snakes(&mut model);
        assert_eq!(pcol(&model), Color::new(0, 85, 0));
        update_snakes(&mut model);
        assert_eq!(pcol(&model), Color::new(0, 76, 0));
        update_snakes(&mut model);
        assert_eq!(pcol(&model), Color::new(0, 66, 0));
        for _ in 0..6 {
            update_snakes(&mut model);
        }
        assert_eq!(model.players[0].body_color().unwrap(), Color::new(0, 9, 0));

        // And eventually it disappears
        update_snakes(&mut model);
        assert!(model.players[0].body_color().is_none());
    }

    fn contains_player_at_pos(
        model: &SnakeModel,
        pos: Point,
        dir: Direction,
    ) -> bool {
        model
            .players
            .iter()
            .find(|p| *p.head() == pos && p.direction == dir)
            .is_some()
    }

    fn add_player<R>(game_state: &mut GameState<R>)
    where
        R: Rng,
    {
        game_state.add_player();
        new_player(
            game_state,
            (game_state.player_settings.view().player_details.len() - 1) as u32,
        )
    }

    fn pt(x: u8, y: u8) -> Point {
        Point { x, y }
    }

    struct FakeRng {
        numbers: Vec<u32>,
    }

    impl FakeRng {
        fn new(numbers: &[u32]) -> Self {
            let mut numbers: Vec<u32> = numbers.into();
            numbers.reverse();
            Self { numbers }
        }
    }

    impl RngCore for FakeRng {
        fn next_u32(&mut self) -> u32 {
            self.numbers.pop().expect("Ran out of numbers!")
        }

        fn next_u64(&mut self) -> u64 {
            todo!()
        }

        fn fill_bytes(&mut self, _dest: &mut [u8]) {
            todo!()
        }

        fn try_fill_bytes(
            &mut self,
            _dest: &mut [u8],
        ) -> Result<(), rand::Error> {
            todo!()
        }
    }

    fn inp(player: u32, button: &str) -> Input {
        Input::PlayerInputDown {
            player,
            button: button.to_owned(),
        }
    }
}
