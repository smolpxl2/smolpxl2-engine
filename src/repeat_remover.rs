use crate::input::Input;
use crate::input_filter::InputFilter;
use std::collections::HashMap;

pub struct RepeatRemover {}

impl RepeatRemover {
    pub fn new() -> Self {
        Self {}
    }
}

impl InputFilter for RepeatRemover {
    fn process(&mut self, inputs: Vec<Input>) -> Vec<Input> {
        let mut ret = Vec::new();
        let mut latest_inputs = HashMap::new();
        for input in inputs {
            let player = input.player();
            let include = match input {
                Input::PlayerInputDown { .. }
                | Input::PlayerInputUp { .. }
                | Input::GeneralInputDown { .. }
                | Input::GeneralInputUp { .. }
                | Input::UnmappedKeyDown { .. }
                | Input::UnmappedKeyUp { .. } => {
                    // Remove this input if it's the same as the last one we
                    // eceived for this player
                    if let Some(latest) = latest_inputs.get(&player) {
                        *latest != input
                    } else {
                        true
                    }
                }
                _ => true,
            };

            if include {
                latest_inputs.insert(player, input.clone());
                ret.push(input);
            }
        }
        ret
    }
}
#[cfg(test)]
mod tests {
    use itertools::Itertools;

    use crate::{
        input::Input, input_processor::InputProcessor,
        repeat_remover::RepeatRemover,
    };

    #[test]
    fn different_inputs_are_preserved() {
        let inputs = vec![
            inp(0, "ArrowUp"),
            inp(0, "ArrowDown"),
            inp(0, "ArrowLeft"),
            inp(0, "ArrowUp"),
            inp(0, "ArrowRight"),
        ];

        let mut input_processor =
            InputProcessor::new().layer(Box::new(RepeatRemover::new()));

        let processed: Vec<_> = input_processor.process(inputs.clone());

        assert_eq!(processed, inputs);
    }

    #[test]
    fn repeated_different_inputs_are_preserved() {
        let inputs = vec![
            inp(0, "ArrowUp"),
            inp(0, "ArrowLeft"),
            inp(0, "ArrowUp"),
            inp(0, "ArrowLeft"),
            inp(0, "ArrowUp"),
            inp(0, "ArrowLeft"),
            inp(0, "ArrowUp"),
        ];

        let mut input_processor =
            InputProcessor::new().layer(Box::new(RepeatRemover::new()));

        let processed: Vec<_> = input_processor.process(inputs.clone());

        assert_eq!(buttons(&processed), buttons(&inputs));
    }

    #[test]
    fn repeated_inputs_are_removed() {
        let inputs = vec![
            inp(0, "ArrowUp"),
            inp(0, "ArrowUp"),
            inp(0, "ArrowLeft"),
            inp(0, "ArrowLeft"),
            inp(0, "ArrowLeft"),
            inp(0, "ArrowUp"),
            inp(0, "ArrowUp"),
        ];

        let mut input_processor =
            InputProcessor::new().layer(Box::new(RepeatRemover::new()));

        let processed: Vec<_> = input_processor.process(inputs);

        assert_eq!(
            processed,
            [inp(0, "ArrowUp"), inp(0, "ArrowLeft"), inp(0, "ArrowUp"),]
        );
    }

    #[test]
    fn non_button_inputs_are_preserved() {
        let inputs = vec![
            player_created(),
            player_created(),
            player_created(),
            player_created(),
        ];

        let mut input_processor =
            InputProcessor::new().layer(Box::new(RepeatRemover::new()));

        let processed: Vec<_> = input_processor.process(inputs);

        assert_eq!(
            processed,
            [
                player_created(),
                player_created(),
                player_created(),
                player_created(),
            ]
        );
    }

    fn buttons<'a>(inputs: impl IntoIterator<Item = &'a Input>) -> String {
        inputs
            .into_iter()
            .map(|input| match input {
                Input::PlayerInputDown { player, button } => {
                    format!("{player}{button}")
                }
                i => format!("{i:?}"),
            })
            .join(" ")
    }

    fn inp(player: u32, button: &str) -> Input {
        Input::PlayerInputDown {
            player,
            button: button.to_owned(),
        }
    }

    fn player_created() -> Input {
        Input::PlayerCreated { player: 0 }
    }
}
