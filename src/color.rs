use image::Rgba;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default, Serialize, Deserialize, PartialEq)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

impl Color {
    pub const fn new(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b }
    }
}

impl From<&Color> for Rgba<u8> {
    fn from(color: &Color) -> Self {
        Rgba::from([color.r, color.g, color.b, 255])
    }
}

impl From<Color> for Rgba<u8> {
    fn from(color: Color) -> Self {
        Rgba::from([color.r, color.g, color.b, 255])
    }
}
