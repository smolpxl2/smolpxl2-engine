# smolpxl2-engine

Smolpxl2 is a game engine for creating retro-style pixellated games in a
browser.

This project is the main game engine (written in Rust). It requires a thin
JavaScript layer that is provided by smolpx2-ui.

## Run

To launch the engine (and a small example game):

```bash
make build
cd pkg
python3 -m http.server
```

Then visit http://0.0.0.0:8000/

## License and credits

Copyright 2023 Andy Balaam and contributors, released under the
[AGPLv3 license](LICENSE) or later.

Contains icons from the
[Feather Icons](https://github.com/feathericons/feather) set, which is
Copyright 2013-2017 Cole Bemis, and released under the
[MIT License](https://github.com/feathericons/feather/blob/8b5d6802fa8fd1eb3924b465ff718d2fa8d61efe/LICENSE).

Uses [shareon](https://shareon.js.org/) by Nikita Karamov to provide the
social sharing buttons.  (The code is dynamically loaded when the Share button
is clicked.)

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md).  By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)

