- Remove "players" level in menu if it's a 1-player game
- Custom menu items (string, colour, choice, number)
- Customise high score and multiplayer server in settings
- Menu items can be clicked
- Menu can be controlled with touch buttons
- Show keys on arrows
- Edit player controls
- Delete player
- Custom player properties (number)
- Custom player properties (choice)
- Custom player properties (colour)
- Save high score table
- Remote play multiplayer snake
- Remember players etc. over restarts (ask if ok)
- Esc to show menu in-game
- High score table
- Show keyboard keys on controls
- Multiplayer playable snake
  + Can control 2 players and it works
  + Different snake colours
  + Don't die if eat apple on own tail
  + Colours on controls
  - Keep score in multiplayer game
- Playable snake
  + fix text positioning
  + die when hitting walls
  + dead colour, persist for 10 steps
  - Restart at end
  - Ready ... go screen
  - Display player names and scores
+ Frame rate limiting
+ Custom player properties (string)
+ Allow editing player names
+ Menu with navigation around levels
+ Title screen
