all: test clippy

test:
	cargo test

test-firefox:
	wasm-pack test --firefox --headless

build:
	cargo build
	wasm-pack build --target=web
	cp -r www/* pkg/
	cp -r lib pkg/

format:
	cargo fmt

clippy:
	cargo clippy

doc:
	cargo doc --open
